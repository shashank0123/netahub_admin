<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmNomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_nominee', function (Blueprint $table) {
            $table->id('nominee_id');
			$table->string('nominee_name');
			$table->string('mobile_no');
			$table->string('nominee_desc');
			$table->string('photo_path');
			$table->string('user_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_nominee');
    }
}

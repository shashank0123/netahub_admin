<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_notification', function (Blueprint $table) {
            $table->id('id');
			$table->string('title');
			$table->string('message');
			$table->string('photo_path');
			$table->string('tone');
			$table->string('user_id');
			$table->string('location_type');
			$table->string('location_id');
			$table->string('cons_id');
			$table->string('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_notification');
    }
}

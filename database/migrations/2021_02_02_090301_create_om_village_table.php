<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmVillageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_village', function (Blueprint $table) {
            $table->id('village_id');
			$table->string('village_name');
			$table->string('block_id');
			$table->string('pincode');
			$table->string('status');
			$table->string('ent_on');
			$table->string('ent_by');
			$table->string('auth_on');
			$table->string('auth_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_village');
    }
}

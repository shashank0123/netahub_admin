<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_user', function (Blueprint $table) {
            $table->id('user_id,');
			$table->string('user_name');
			$table->string('display_name');
			$table->string('user_password');
			$table->string('email_id');
			$table->string('mobile_no');
			$table->string('dateofbirth');
			$table->string('qualification');
			$table->string('occupation');
			$table->string('gender');
			$table->string('photo_path');
			$table->string('college_id');
			$table->string('society_id');
			$table->string('ward_id');
			$table->string('village_id');
			$table->string('block_id');
			$table->string('tehsil_id');
			$table->string('district_id');
			$table->string('state_id');
			$table->string('country_id');
			$table->string('token_id');
			$table->string('pincode');
			$table->string('val_cons');
			$table->string('referred_by');
			$table->string('status');
			$table->string('ent_on');
			$table->string('ent_by');
			$table->string('auth_on');
			$table->string('auth_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_user');
    }
}

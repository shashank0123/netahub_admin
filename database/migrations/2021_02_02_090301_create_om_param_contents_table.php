<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmParamContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_param_contents', function (Blueprint $table) {
            $table->id('id');
			$table->string('param_id');
			$table->string('parent_id');
			$table->string('param_type');
			$table->string('content_type');
			$table->string('content_detail');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_param_contents');
    }
}

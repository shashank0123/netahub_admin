<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTtUserPointDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_user_point_det', function (Blueprint $table) {
            $table->id('id');
			$table->string('user_id');
			$table->string('log_date');
			$table->string('trns_type');
			$table->string('point');
			$table->string('remarks');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_user_point_det');
    }
}

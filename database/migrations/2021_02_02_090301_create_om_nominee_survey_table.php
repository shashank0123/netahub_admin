<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmNomineeSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_nominee_survey', function (Blueprint $table) {
            $table->id('id');
			$table->string('survey_id');
			$table->string('cons_id');
			$table->string('nominee_id');
			$table->string('party_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_nominee_survey');
    }
}

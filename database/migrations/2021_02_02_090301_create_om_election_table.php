<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmElectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_election', function (Blueprint $table) {
            $table->id('election_id');
			$table->string('election_name');
			$table->string('election_image');
			$table->string('election_description');
			$table->string('election_type');
			$table->string('location_type');
			$table->string('type');
			$table->string('req_point');
			$table->string('status');
			$table->string('ent_on');
			$table->string('ent_by');
			$table->string('auth_on');
			$table->string('auth_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_election');
    }
}

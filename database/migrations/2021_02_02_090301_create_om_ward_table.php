<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOmWardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('om_ward', function (Blueprint $table) {
            $table->id('ward_id,');
			$table->string('ward_name');
			$table->string('village_id');
			$table->string('status');
			$table->string('ent_on');
			$table->string('ent_by');
			$table->string('auth_on');
			$table->string('auth_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('om_ward');
    }
}

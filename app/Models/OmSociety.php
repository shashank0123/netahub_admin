<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmSociety extends Model
{
    use HasFactory;
    protected $primaryKey = 'society_id';
    protected $table = 'om_society';
    public $timestamps = false;
    protected $fillable = [
			'society_name',
			'district_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmNomineeSurvey extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_nominee_survey';
    public $timestamps = false;
    protected $fillable = [
			'survey_id',
			'cons_id',
			'nominee_id',
			'party_id',
		];
}

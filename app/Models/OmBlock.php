<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmBlock extends Model
{
    use HasFactory;
    protected $primaryKey = 'block_id';
    protected $table = 'om_block';
    public $timestamps = false;
    protected $fillable = [
			'block_name',
			'tehsil_id',
			'block_type',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

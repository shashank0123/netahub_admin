<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmParty extends Model
{
    use HasFactory;
    protected $primaryKey = 'party_id,';
    protected $table = 'om_party';
    public $timestamps = false;
    protected $fillable = [
			'party_name',
			'party_code',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmConstituency extends Model
{
    use HasFactory;
    protected $primaryKey = 'cons_id';
    protected $table = 'om_constituency';
    public $timestamps = false;
    protected $fillable = [
			'cons_name',
			'election_id',
			'ref_id',
		];
}

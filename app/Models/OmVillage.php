<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmVillage extends Model
{
    use HasFactory;
    protected $primaryKey = 'village_id';
    protected $table = 'om_village';
    public $timestamps = false;
    protected $fillable = [
			'village_name',
			'block_id',
			'pincode',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

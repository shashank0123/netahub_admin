<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmNominee extends Model
{
    use HasFactory;
    protected $primaryKey = 'nominee_id';
    protected $table = 'om_nominee';
    public $timestamps = false;
    protected $fillable = [
			'nominee_name',
			'mobile_no',
			'nominee_desc',
			'photo_path',
			'user_id',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtFeedback extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'tt_feedback';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'log_date',
			'feedback_desc',
			'status',
		];
}

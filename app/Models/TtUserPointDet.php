<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtUserPointDet extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'tt_user_point_det';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'log_date',
			'trns_type',
			'point',
			'remarks',
		];
}

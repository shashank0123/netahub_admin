<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmUserConsMap extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_user_cons_map';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'cons_id',
			'election_id',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmDistrict extends Model
{
    use HasFactory;
    protected $primaryKey = 'district_id';
    protected $table = 'om_district';
    public $timestamps = false;
    protected $fillable = [
			'district_name',
			'state_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmElectionParam extends Model
{
    use HasFactory;
    protected $primaryKey = 'param_id';
    protected $table = 'om_election_param';
    public $timestamps = false;
    protected $fillable = [
			'param_code',
			'param_desc',
		];
}

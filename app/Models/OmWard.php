<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmWard extends Model
{
    use HasFactory;
    protected $primaryKey = 'ward_id,';
    protected $table = 'om_ward';
    public $timestamps = false;
    protected $fillable = [
			'ward_name',
			'village_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

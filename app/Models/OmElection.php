<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmElection extends Model
{
    use HasFactory;
    protected $primaryKey = 'election_id';
    protected $table = 'om_election';
    public $timestamps = false;
    protected $fillable = [
			'election_name',
			'election_image',
			'election_description',
			'election_type',
			'location_type',
			'type',
			'req_point',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

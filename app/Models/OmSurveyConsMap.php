<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmSurveyConsMap extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_survey_cons_map';
    public $timestamps = false;
    protected $fillable = [
			'survey_id',
			'cons_id',
		];
}

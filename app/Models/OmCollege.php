<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmCollege extends Model
{
    use HasFactory;
    protected $primaryKey = 'college_id';
    protected $table = 'om_college';
    public $timestamps = false;
    protected $fillable = [
			'college_name',
			'state_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

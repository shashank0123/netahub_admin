<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TtNotification extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'tt_notification';
    public $timestamps = false;
    protected $fillable = [
			'title',
			'message',
			'photo_path',
			'tone',
			'user_id',
			'location_type',
			'location_id',
			'cons_id',
			'status',
		];
}

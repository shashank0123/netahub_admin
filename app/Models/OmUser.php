<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmUser extends Model
{
    use HasFactory;
    protected $primaryKey = 'user_id,';
    protected $table = 'om_user';
    public $timestamps = false;
    protected $fillable = [
			'user_name',
			'display_name',
			'user_password',
			'email_id',
			'mobile_no',
			'dateofbirth',
			'qualification',
			'occupation',
			'gender',
			'photo_path',
			'college_id',
			'society_id',
			'ward_id',
			'village_id',
			'block_id',
			'tehsil_id',
			'district_id',
			'state_id',
			'country_id',
			'token_id',
			'pincode',
			'val_cons',
			'referred_by',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

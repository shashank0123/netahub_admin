<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmUserDevice extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_user_device';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'token_id',
			'imei',
			'model',
			'status',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmState extends Model
{
    use HasFactory;
    protected $primaryKey = 'state_id,';
    protected $table = 'om_state';
    public $timestamps = false;
    protected $fillable = [
			'state_name',
			'country_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmSurvey extends Model
{
    use HasFactory;
    protected $primaryKey = 'survey_id,';
    protected $table = 'om_survey';
    public $timestamps = false;
    protected $fillable = [
			'survey_name',
			'from_date',
			'to_date',
			'election_id',
		];
}

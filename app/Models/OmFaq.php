<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmFaq extends Model
{
    use HasFactory;
    protected $primaryKey = 'faq_id,';
    protected $table = 'om_faq';
    public $timestamps = false;
    protected $fillable = [
			'question',
			'answer',
			'priority',
			'status',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmUserinformation extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_user_information';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'user_information',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmCountry extends Model
{
    use HasFactory;
    protected $primaryKey = 'country_id';
    protected $table = 'om_country';
    public $timestamps = false;
    protected $fillable = [
			'country_name',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

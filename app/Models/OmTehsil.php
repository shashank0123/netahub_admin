<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmTehsil extends Model
{
    use HasFactory;
    protected $primaryKey = 'tehsil_id,';
    protected $table = 'om_tehsil';
    public $timestamps = false;
    protected $fillable = [
			'tehsil_name',
			'district_id',
			'status',
			'ent_on',
			'ent_by',
			'auth_on',
			'auth_by',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OmParamContents extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'om_param_contents';
    public $timestamps = false;
    protected $fillable = [
			'param_id',
			'parent_id',
			'param_type',
			'content_type',
			'content_detail',
		];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ttUserPointHdr extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'tt_user_point_hdr';
    public $timestamps = false;
    protected $fillable = [
			'user_id',
			'point',
			'remarks',
		];
}

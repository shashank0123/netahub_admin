<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmUser;
use Illuminate\Support\Facades\DB;

//add models here

class OmUserController extends Controller
{
    public function index(){
        $om_user = OmUser::paginate(30);
        return view('admin.om_user.all', compact('om_user'));
    }

    public function create(){

        $om_college = DB::table('om_college')->get();
$om_society = DB::table('om_society')->get();
$om_ward = DB::table('om_ward')->get();
$om_village = DB::table('om_village')->get();
$om_block = DB::table('om_block')->get();
$om_tehsil = DB::table('om_tehsil')->get();
$om_district = DB::table('om_district')->get();
$om_state = DB::table('om_state')->get();
$om_country = DB::table('om_country')->get();

        return view('admin.om_user.edit', compact('om_college','om_society','om_ward','om_village','om_block','om_tehsil','om_district','om_state','om_country'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_name'] = $data['user_name'];
		$saveData['display_name'] = $data['display_name'];
		$saveData['user_password'] = $data['user_password'];
		$saveData['email_id'] = $data['email_id'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['dateofbirth'] = $data['dateofbirth'];
		$saveData['qualification'] = $data['qualification'];
		$saveData['occupation'] = $data['occupation'];
		$saveData['gender'] = $data['gender'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['college_id'] = $data['college_id'];
		$saveData['society_id'] = $data['society_id'];
		$saveData['ward_id'] = $data['ward_id'];
		$saveData['village_id'] = $data['village_id'];
		$saveData['block_id'] = $data['block_id'];
		$saveData['tehsil_id'] = $data['tehsil_id'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['country_id'] = $data['country_id'];
		$saveData['token_id'] = $data['token_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['val_cons'] = $data['val_cons'];
		$saveData['referred_by'] = $data['referred_by'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_user = OmUser::create($saveData);

        return redirect('/admin/om_user')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_user = OmUser::where('user_id,', $id)->first();
        $om_college = DB::table('om_college')->get();
$om_society = DB::table('om_society')->get();
$om_ward = DB::table('om_ward')->get();
$om_village = DB::table('om_village')->get();
$om_block = DB::table('om_block')->get();
$om_tehsil = DB::table('om_tehsil')->get();
$om_district = DB::table('om_district')->get();
$om_state = DB::table('om_state')->get();
$om_country = DB::table('om_country')->get();

        return view('admin.om_user.edit', compact('om_user','om_college','om_society','om_ward','om_village','om_block','om_tehsil','om_district','om_state','om_country'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_name'] = $data['user_name'];
		$saveData['display_name'] = $data['display_name'];
		$saveData['user_password'] = $data['user_password'];
		$saveData['email_id'] = $data['email_id'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['dateofbirth'] = $data['dateofbirth'];
		$saveData['qualification'] = $data['qualification'];
		$saveData['occupation'] = $data['occupation'];
		$saveData['gender'] = $data['gender'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['college_id'] = $data['college_id'];
		$saveData['society_id'] = $data['society_id'];
		$saveData['ward_id'] = $data['ward_id'];
		$saveData['village_id'] = $data['village_id'];
		$saveData['block_id'] = $data['block_id'];
		$saveData['tehsil_id'] = $data['tehsil_id'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['country_id'] = $data['country_id'];
		$saveData['token_id'] = $data['token_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['val_cons'] = $data['val_cons'];
		$saveData['referred_by'] = $data['referred_by'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmUser::where('user_id,', $id)->first();
        if ($row){
            $OmUser = OmUser::where('user_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_user')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmUser::where('user_id,', $request->id)->delete();
        return redirect('/admin/om_user');

    }
}

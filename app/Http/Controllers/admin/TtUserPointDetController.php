<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TtUserPointDet;
use Illuminate\Support\Facades\DB;

//add models here

class TtUserPointDetController extends Controller
{
    public function index(){
        $tt_user_point_det = TtUserPointDet::paginate(30);
        return view('admin.tt_user_point_det.all', compact('tt_user_point_det'));
    }

    public function create(){

        
        return view('admin.tt_user_point_det.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['trns_type'] = $data['trns_type'];
		$saveData['point'] = $data['point'];
		$saveData['remarks'] = $data['remarks'];

        $tt_user_point_det = TtUserPointDet::create($saveData);

        return redirect('/admin/tt_user_point_det')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $tt_user_point_det = TtUserPointDet::where('id', $id)->first();
        
        return view('admin.tt_user_point_det.edit', compact('tt_user_point_det'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['trns_type'] = $data['trns_type'];
		$saveData['point'] = $data['point'];
		$saveData['remarks'] = $data['remarks'];

        $row = TtUserPointDet::where('id', $id)->first();
        if ($row){
            $TtUserPointDet = TtUserPointDet::where('id', $id)->update($saveData);
        }

        return redirect('/admin/tt_user_point_det')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = TtUserPointDet::where('id', $request->id)->delete();
        return redirect('/admin/tt_user_point_det');

    }
}

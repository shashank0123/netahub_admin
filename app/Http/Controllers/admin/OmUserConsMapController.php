<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmUserConsMap;
use Illuminate\Support\Facades\DB;

//add models here

class OmUserConsMapController extends Controller
{
    public function index(){
        $om_user_cons_map = OmUserConsMap::paginate(30);
        return view('admin.om_user_cons_map.all', compact('om_user_cons_map'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();
$om_constituency = DB::table('om_constituency')->get();
$om_election = DB::table('om_election')->get();

        return view('admin.om_user_cons_map.edit', compact('om_user','om_constituency','om_election'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['election_id'] = $data['election_id'];

        $om_user_cons_map = OmUserConsMap::create($saveData);

        return redirect('/admin/om_user_cons_map')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_user_cons_map = OmUserConsMap::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();
$om_constituency = DB::table('om_constituency')->get();
$om_election = DB::table('om_election')->get();

        return view('admin.om_user_cons_map.edit', compact('om_user_cons_map','om_user','om_constituency','om_election'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['election_id'] = $data['election_id'];

        $row = OmUserConsMap::where('id', $id)->first();
        if ($row){
            $OmUserConsMap = OmUserConsMap::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_user_cons_map')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmUserConsMap::where('id', $request->id)->delete();
        return redirect('/admin/om_user_cons_map');

    }
}

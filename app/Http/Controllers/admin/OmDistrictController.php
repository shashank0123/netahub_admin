<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmDistrict;
use Illuminate\Support\Facades\DB;

//add models here

class OmDistrictController extends Controller
{
    public function index(){
        $om_district = OmDistrict::paginate(30);
        return view('admin.om_district.all', compact('om_district'));
    }

    public function create(){

        $om_state = DB::table('om_state')->get();

        return view('admin.om_district.edit', compact('om_state'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['district_name'] = $data['district_name'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_district = OmDistrict::create($saveData);

        return redirect('/admin/om_district')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_district = OmDistrict::where('district_id', $id)->first();
        $om_state = DB::table('om_state')->get();

        return view('admin.om_district.edit', compact('om_district','om_state'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['district_name'] = $data['district_name'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmDistrict::where('district_id', $id)->first();
        if ($row){
            $OmDistrict = OmDistrict::where('district_id', $id)->update($saveData);
        }

        return redirect('/admin/om_district')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmDistrict::where('district_id', $request->id)->delete();
        return redirect('/admin/om_district');

    }
}

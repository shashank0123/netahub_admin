<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmUserinformation;
use Illuminate\Support\Facades\DB;

//add models here

class OmUserinformationController extends Controller
{
    public function index(){
        $om_user_information = OmUserinformation::paginate(30);
        return view('admin.om_user_information.all', compact('om_user_information'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();

        return view('admin.om_user_information.edit', compact('om_user'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['user_information'] = $data['user_information'];

        $om_user_information = OmUserinformation::create($saveData);

        return redirect('/admin/om_user_information')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_user_information = OmUserinformation::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();

        return view('admin.om_user_information.edit', compact('om_user_information','om_user'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['user_information'] = $data['user_information'];

        $row = OmUserinformation::where('id', $id)->first();
        if ($row){
            $OmUserinformation = OmUserinformation::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_user_information')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmUserinformation::where('id', $request->id)->delete();
        return redirect('/admin/om_user_information');

    }
}

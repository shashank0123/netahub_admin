<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmParamContents;
use Illuminate\Support\Facades\DB;

//add models here

class OmParamContentsController extends Controller
{
    public function index(){
        $om_param_contents = OmParamContents::paginate(30);
        return view('admin.om_param_contents.all', compact('om_param_contents'));
    }

    public function create(){

        $om_state = DB::table('om_state')->get();
$om_country = DB::table('om_country')->get();

        return view('admin.om_param_contents.edit', compact('om_state','om_country'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['param_id'] = $data['param_id'];
		$saveData[' parent_id'] = $data[' parent_id'];
		$saveData[' param_type'] = $data[' param_type'];
		$saveData[' content_type'] = $data[' content_type'];
		$saveData[' content_detail'] = $data[' content_detail'];

        $om_param_contents = OmParamContents::create($saveData);

        return redirect('/admin/om_param_contents')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_param_contents = OmParamContents::where('id', $id)->first();
        $om_state = DB::table('om_state')->get();
$om_country = DB::table('om_country')->get();

        return view('admin.om_param_contents.edit', compact('om_param_contents','om_state','om_country'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['param_id'] = $data['param_id'];
		$saveData[' parent_id'] = $data[' parent_id'];
		$saveData[' param_type'] = $data[' param_type'];
		$saveData[' content_type'] = $data[' content_type'];
		$saveData[' content_detail'] = $data[' content_detail'];

        $row = OmParamContents::where('id', $id)->first();
        if ($row){
            $OmParamContents = OmParamContents::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_param_contents')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmParamContents::where('id', $request->id)->delete();
        return redirect('/admin/om_param_contents');

    }
}

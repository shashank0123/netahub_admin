<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TtNotification;
use Illuminate\Support\Facades\DB;

//add models here

class TtNotificationController extends Controller
{
    public function index(){
        $tt_notification = TtNotification::paginate(30);
        return view('admin.tt_notification.all', compact('tt_notification'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();
$om_constituency = DB::table('om_constituency')->get();

        return view('admin.tt_notification.edit', compact('om_user','om_constituency'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['title'] = $data['title'];
		$saveData['message'] = $data['message'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['tone'] = $data['tone'];
		$saveData['user_id'] = $data['user_id'];
		$saveData['location_type'] = $data['location_type'];
		$saveData['location_id'] = $data['location_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['status'] = $data['status'];

        $tt_notification = TtNotification::create($saveData);

        return redirect('/admin/tt_notification')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $tt_notification = TtNotification::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();
$om_constituency = DB::table('om_constituency')->get();

        return view('admin.tt_notification.edit', compact('tt_notification','om_user','om_constituency'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['title'] = $data['title'];
		$saveData['message'] = $data['message'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['tone'] = $data['tone'];
		$saveData['user_id'] = $data['user_id'];
		$saveData['location_type'] = $data['location_type'];
		$saveData['location_id'] = $data['location_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['status'] = $data['status'];

        $row = TtNotification::where('id', $id)->first();
        if ($row){
            $TtNotification = TtNotification::where('id', $id)->update($saveData);
        }

        return redirect('/admin/tt_notification')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = TtNotification::where('id', $request->id)->delete();
        return redirect('/admin/tt_notification');

    }
}

<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmConstituency;
use Illuminate\Support\Facades\DB;

//add models here

class OmConstituencyController extends Controller
{
    public function index(){
        $om_constituency = OmConstituency::paginate(30);
        return view('admin.om_constituency.all', compact('om_constituency'));
    }

    public function create(){

        $om_election = DB::table('om_election')->get();

        return view('admin.om_constituency.edit', compact('om_election'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['cons_name'] = $data['cons_name'];
		$saveData['election_id'] = $data['election_id'];
		$saveData['ref_id'] = $data['ref_id'];

        $om_constituency = OmConstituency::create($saveData);

        return redirect('/admin/om_constituency')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_constituency = OmConstituency::where('cons_id', $id)->first();
        $om_election = DB::table('om_election')->get();

        return view('admin.om_constituency.edit', compact('om_constituency','om_election'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['cons_name'] = $data['cons_name'];
		$saveData['election_id'] = $data['election_id'];
		$saveData['ref_id'] = $data['ref_id'];

        $row = OmConstituency::where('cons_id', $id)->first();
        if ($row){
            $OmConstituency = OmConstituency::where('cons_id', $id)->update($saveData);
        }

        return redirect('/admin/om_constituency')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmConstituency::where('cons_id', $request->id)->delete();
        return redirect('/admin/om_constituency');

    }
}

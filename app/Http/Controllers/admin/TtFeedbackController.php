<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\TtFeedback;
use Illuminate\Support\Facades\DB;

//add models here

class TtFeedbackController extends Controller
{
    public function index(){
        $tt_feedback = TtFeedback::paginate(30);
        return view('admin.tt_feedback.all', compact('tt_feedback'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();

        return view('admin.tt_feedback.edit', compact('om_user'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['feedback_desc'] = $data['feedback_desc'];
		$saveData['status'] = $data['status'];

        $tt_feedback = TtFeedback::create($saveData);

        return redirect('/admin/tt_feedback')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $tt_feedback = TtFeedback::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();

        return view('admin.tt_feedback.edit', compact('tt_feedback','om_user'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['log_date'] = $data['log_date'];
		$saveData['feedback_desc'] = $data['feedback_desc'];
		$saveData['status'] = $data['status'];

        $row = TtFeedback::where('id', $id)->first();
        if ($row){
            $TtFeedback = TtFeedback::where('id', $id)->update($saveData);
        }

        return redirect('/admin/tt_feedback')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = TtFeedback::where('id', $request->id)->delete();
        return redirect('/admin/tt_feedback');

    }
}

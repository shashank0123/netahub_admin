<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmWard;
use Illuminate\Support\Facades\DB;

//add models here

class OmWardController extends Controller
{
    public function index(){
        $om_ward = OmWard::paginate(30);
        return view('admin.om_ward.all', compact('om_ward'));
    }

    public function create(){

        $om_village = DB::table('om_village')->get();

        return view('admin.om_ward.edit', compact('om_village'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['ward_name'] = $data['ward_name'];
		$saveData['village_id'] = $data['village_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_ward = OmWard::create($saveData);

        return redirect('/admin/om_ward')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_ward = OmWard::where('ward_id,', $id)->first();
        $om_village = DB::table('om_village')->get();

        return view('admin.om_ward.edit', compact('om_ward','om_village'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['ward_name'] = $data['ward_name'];
		$saveData['village_id'] = $data['village_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmWard::where('ward_id,', $id)->first();
        if ($row){
            $OmWard = OmWard::where('ward_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_ward')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmWard::where('ward_id,', $request->id)->delete();
        return redirect('/admin/om_ward');

    }
}

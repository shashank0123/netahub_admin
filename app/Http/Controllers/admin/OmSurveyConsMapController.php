<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmSurveyConsMap;
use Illuminate\Support\Facades\DB;

//add models here

class OmSurveyConsMapController extends Controller
{
    public function index(){
        $om_survey_cons_map = OmSurveyConsMap::paginate(30);
        return view('admin.om_survey_cons_map.all', compact('om_survey_cons_map'));
    }

    public function create(){

        $om_survey = DB::table('om_survey')->get();
$om_constituency = DB::table('om_constituency')->get();

        return view('admin.om_survey_cons_map.edit', compact('om_survey','om_constituency'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_id'] = $data['survey_id'];
		$saveData['cons_id'] = $data['cons_id'];

        $om_survey_cons_map = OmSurveyConsMap::create($saveData);

        return redirect('/admin/om_survey_cons_map')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_survey_cons_map = OmSurveyConsMap::where('id', $id)->first();
        $om_survey = DB::table('om_survey')->get();
$om_constituency = DB::table('om_constituency')->get();

        return view('admin.om_survey_cons_map.edit', compact('om_survey_cons_map','om_survey','om_constituency'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_id'] = $data['survey_id'];
		$saveData['cons_id'] = $data['cons_id'];

        $row = OmSurveyConsMap::where('id', $id)->first();
        if ($row){
            $OmSurveyConsMap = OmSurveyConsMap::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_survey_cons_map')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmSurveyConsMap::where('id', $request->id)->delete();
        return redirect('/admin/om_survey_cons_map');

    }
}

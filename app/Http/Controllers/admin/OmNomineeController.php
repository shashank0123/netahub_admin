<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmNominee;
use Illuminate\Support\Facades\DB;

//add models here

class OmNomineeController extends Controller
{
    public function index(){
        $om_nominee = OmNominee::paginate(30);
        return view('admin.om_nominee.all', compact('om_nominee'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();

        return view('admin.om_nominee.edit', compact('om_user'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['nominee_name'] = $data['nominee_name'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['nominee_desc'] = $data['nominee_desc'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['user_id'] = $data['user_id'];

        $om_nominee = OmNominee::create($saveData);

        return redirect('/admin/om_nominee')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_nominee = OmNominee::where('nominee_id', $id)->first();
        $om_user = DB::table('om_user')->get();

        return view('admin.om_nominee.edit', compact('om_nominee','om_user'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['nominee_name'] = $data['nominee_name'];
		$saveData['mobile_no'] = $data['mobile_no'];
		$saveData['nominee_desc'] = $data['nominee_desc'];
		$saveData['photo_path'] = $data['photo_path'];
		$saveData['user_id'] = $data['user_id'];

        $row = OmNominee::where('nominee_id', $id)->first();
        if ($row){
            $OmNominee = OmNominee::where('nominee_id', $id)->update($saveData);
        }

        return redirect('/admin/om_nominee')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmNominee::where('nominee_id', $request->id)->delete();
        return redirect('/admin/om_nominee');

    }
}

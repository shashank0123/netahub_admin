<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmParty;
use Illuminate\Support\Facades\DB;

//add models here

class OmPartyController extends Controller
{
    public function index(){
        $om_party = OmParty::paginate(30);
        return view('admin.om_party.all', compact('om_party'));
    }

    public function create(){

        
        return view('admin.om_party.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['party_name'] = $data['party_name'];
		$saveData['party_code'] = $data['party_code'];

        $om_party = OmParty::create($saveData);

        return redirect('/admin/om_party')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_party = OmParty::where('party_id,', $id)->first();
        
        return view('admin.om_party.edit', compact('om_party'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['party_name'] = $data['party_name'];
		$saveData['party_code'] = $data['party_code'];

        $row = OmParty::where('party_id,', $id)->first();
        if ($row){
            $OmParty = OmParty::where('party_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_party')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmParty::where('party_id,', $request->id)->delete();
        return redirect('/admin/om_party');

    }
}

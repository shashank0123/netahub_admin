<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmElection;
use Illuminate\Support\Facades\DB;

//add models here

class OmElectionController extends Controller
{
    public function index(){
        $om_election = OmElection::paginate(30);
        return view('admin.om_election.all', compact('om_election'));
    }

    public function create(){

        
        return view('admin.om_election.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['election_name'] = $data['election_name'];
		$saveData['election_image'] = $data['election_image'];
		$saveData['election_description'] = $data['election_description'];
		$saveData['election_type'] = $data['election_type'];
		$saveData['location_type'] = $data['location_type'];
		$saveData['type'] = $data['type'];
		$saveData['req_point'] = $data['req_point'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_election = OmElection::create($saveData);

        return redirect('/admin/om_election')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_election = OmElection::where('election_id', $id)->first();
        
        return view('admin.om_election.edit', compact('om_election'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['election_name'] = $data['election_name'];
		$saveData['election_image'] = $data['election_image'];
		$saveData['election_description'] = $data['election_description'];
		$saveData['election_type'] = $data['election_type'];
		$saveData['location_type'] = $data['location_type'];
		$saveData['type'] = $data['type'];
		$saveData['req_point'] = $data['req_point'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmElection::where('election_id', $id)->first();
        if ($row){
            $OmElection = OmElection::where('election_id', $id)->update($saveData);
        }

        return redirect('/admin/om_election')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmElection::where('election_id', $request->id)->delete();
        return redirect('/admin/om_election');

    }
}

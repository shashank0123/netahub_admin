<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmElectionParam;
use Illuminate\Support\Facades\DB;

//add models here

class OmElectionParamController extends Controller
{
    public function index(){
        $om_election_param = OmElectionParam::paginate(30);
        return view('admin.om_election_param.all', compact('om_election_param'));
    }

    public function create(){

        
        return view('admin.om_election_param.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['param_code'] = $data['param_code'];
		$saveData['param_desc'] = $data['param_desc'];

        $om_election_param = OmElectionParam::create($saveData);

        return redirect('/admin/om_election_param')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_election_param = OmElectionParam::where('param_id', $id)->first();
        
        return view('admin.om_election_param.edit', compact('om_election_param'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['param_code'] = $data['param_code'];
		$saveData['param_desc'] = $data['param_desc'];

        $row = OmElectionParam::where('param_id', $id)->first();
        if ($row){
            $OmElectionParam = OmElectionParam::where('param_id', $id)->update($saveData);
        }

        return redirect('/admin/om_election_param')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmElectionParam::where('param_id', $request->id)->delete();
        return redirect('/admin/om_election_param');

    }
}

<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmTehsil;
use Illuminate\Support\Facades\DB;

//add models here

class OmTehsilController extends Controller
{
    public function index(){
        $om_tehsil = OmTehsil::paginate(30);
        return view('admin.om_tehsil.all', compact('om_tehsil'));
    }

    public function create(){

        $om_district = DB::table('om_district')->get();

        return view('admin.om_tehsil.edit', compact('om_district'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['tehsil_name'] = $data['tehsil_name'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_tehsil = OmTehsil::create($saveData);

        return redirect('/admin/om_tehsil')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_tehsil = OmTehsil::where('tehsil_id,', $id)->first();
        $om_district = DB::table('om_district')->get();

        return view('admin.om_tehsil.edit', compact('om_tehsil','om_district'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['tehsil_name'] = $data['tehsil_name'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmTehsil::where('tehsil_id,', $id)->first();
        if ($row){
            $OmTehsil = OmTehsil::where('tehsil_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_tehsil')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmTehsil::where('tehsil_id,', $request->id)->delete();
        return redirect('/admin/om_tehsil');

    }
}

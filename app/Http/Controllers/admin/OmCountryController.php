<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmCountry;
use Illuminate\Support\Facades\DB;

//add models here

class OmCountryController extends Controller
{
    public function index(){
        $om_country = OmCountry::paginate(30);
        return view('admin.om_country.all', compact('om_country'));
    }

    public function create(){

        
        return view('admin.om_country.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['country_name'] = $data['country_name'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_country = OmCountry::create($saveData);

        return redirect('/admin/om_country')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_country = OmCountry::where('country_id', $id)->first();
        
        return view('admin.om_country.edit', compact('om_country'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['country_name'] = $data['country_name'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmCountry::where('country_id', $id)->first();
        if ($row){
            $OmCountry = OmCountry::where('country_id', $id)->update($saveData);
        }

        return redirect('/admin/om_country')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmCountry::where('country_id', $request->id)->delete();
        return redirect('/admin/om_country');

    }
}

<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmSurvey;
use Illuminate\Support\Facades\DB;

//add models here

class OmSurveyController extends Controller
{
    public function index(){
        $om_survey = OmSurvey::paginate(30);
        return view('admin.om_survey.all', compact('om_survey'));
    }

    public function create(){

        $om_election = DB::table('om_election')->get();

        return view('admin.om_survey.edit', compact('om_election'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_name'] = $data['survey_name'];
		$saveData['from_date'] = $data['from_date'];
		$saveData['to_date'] = $data['to_date'];
		$saveData['election_id'] = $data['election_id'];

        $om_survey = OmSurvey::create($saveData);

        return redirect('/admin/om_survey')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_survey = OmSurvey::where('survey_id,', $id)->first();
        $om_election = DB::table('om_election')->get();

        return view('admin.om_survey.edit', compact('om_survey','om_election'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_name'] = $data['survey_name'];
		$saveData['from_date'] = $data['from_date'];
		$saveData['to_date'] = $data['to_date'];
		$saveData['election_id'] = $data['election_id'];

        $row = OmSurvey::where('survey_id,', $id)->first();
        if ($row){
            $OmSurvey = OmSurvey::where('survey_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_survey')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmSurvey::where('survey_id,', $request->id)->delete();
        return redirect('/admin/om_survey');

    }
}

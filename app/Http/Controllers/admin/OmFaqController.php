<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmFaq;
use Illuminate\Support\Facades\DB;

//add models here

class OmFaqController extends Controller
{
    public function index(){
        $om_faq = OmFaq::paginate(30);
        return view('admin.om_faq.all', compact('om_faq'));
    }

    public function create(){

        
        return view('admin.om_faq.edit', compact());
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['question'] = $data['question'];
		$saveData['answer'] = $data['answer'];
		$saveData['priority'] = $data['priority'];
		$saveData['status'] = $data['status'];

        $om_faq = OmFaq::create($saveData);

        return redirect('/admin/om_faq')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_faq = OmFaq::where('faq_id,', $id)->first();
        
        return view('admin.om_faq.edit', compact('om_faq'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['question'] = $data['question'];
		$saveData['answer'] = $data['answer'];
		$saveData['priority'] = $data['priority'];
		$saveData['status'] = $data['status'];

        $row = OmFaq::where('faq_id,', $id)->first();
        if ($row){
            $OmFaq = OmFaq::where('faq_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_faq')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmFaq::where('faq_id,', $request->id)->delete();
        return redirect('/admin/om_faq');

    }
}

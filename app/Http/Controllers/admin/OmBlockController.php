<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmBlock;
use Illuminate\Support\Facades\DB;

//add models here

class OmBlockController extends Controller
{
    public function index(){
        $om_block = OmBlock::paginate(30);
        return view('admin.om_block.all', compact('om_block'));
    }

    public function create(){

        $om_tehsil = DB::table('om_tehsil')->get();

        return view('admin.om_block.edit', compact('om_tehsil'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['block_name'] = $data['block_name'];
		$saveData['tehsil_id'] = $data['tehsil_id'];
		$saveData['block_type'] = $data['block_type'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_block = OmBlock::create($saveData);

        return redirect('/admin/om_block')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_block = OmBlock::where('block_id', $id)->first();
        $om_tehsil = DB::table('om_tehsil')->get();

        return view('admin.om_block.edit', compact('om_block','om_tehsil'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['block_name'] = $data['block_name'];
		$saveData['tehsil_id'] = $data['tehsil_id'];
		$saveData['block_type'] = $data['block_type'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmBlock::where('block_id', $id)->first();
        if ($row){
            $OmBlock = OmBlock::where('block_id', $id)->update($saveData);
        }

        return redirect('/admin/om_block')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmBlock::where('block_id', $request->id)->delete();
        return redirect('/admin/om_block');

    }
}

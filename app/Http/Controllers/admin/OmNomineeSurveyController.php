<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmNomineeSurvey;
use Illuminate\Support\Facades\DB;

//add models here

class OmNomineeSurveyController extends Controller
{
    public function index(){
        $om_nominee_survey = OmNomineeSurvey::paginate(30);
        return view('admin.om_nominee_survey.all', compact('om_nominee_survey'));
    }

    public function create(){

        $om_survey = DB::table('om_survey')->get();
$om_constituency = DB::table('om_constituency')->get();
$om_nominee = DB::table('om_nominee')->get();
$om_party = DB::table('om_party')->get();

        return view('admin.om_nominee_survey.edit', compact('om_survey','om_constituency','om_nominee','om_party'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_id'] = $data['survey_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['nominee_id'] = $data['nominee_id'];
		$saveData['party_id'] = $data['party_id'];

        $om_nominee_survey = OmNomineeSurvey::create($saveData);

        return redirect('/admin/om_nominee_survey')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_nominee_survey = OmNomineeSurvey::where('id', $id)->first();
        $om_survey = DB::table('om_survey')->get();
$om_constituency = DB::table('om_constituency')->get();
$om_nominee = DB::table('om_nominee')->get();
$om_party = DB::table('om_party')->get();

        return view('admin.om_nominee_survey.edit', compact('om_nominee_survey','om_survey','om_constituency','om_nominee','om_party'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['survey_id'] = $data['survey_id'];
		$saveData['cons_id'] = $data['cons_id'];
		$saveData['nominee_id'] = $data['nominee_id'];
		$saveData['party_id'] = $data['party_id'];

        $row = OmNomineeSurvey::where('id', $id)->first();
        if ($row){
            $OmNomineeSurvey = OmNomineeSurvey::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_nominee_survey')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmNomineeSurvey::where('id', $request->id)->delete();
        return redirect('/admin/om_nominee_survey');

    }
}

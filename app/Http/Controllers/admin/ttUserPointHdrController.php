<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\ttUserPointHdr;
use Illuminate\Support\Facades\DB;

//add models here

class ttUserPointHdrController extends Controller
{
    public function index(){
        $tt_user_point_hdr = ttUserPointHdr::paginate(30);
        return view('admin.tt_user_point_hdr.all', compact('tt_user_point_hdr'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();

        return view('admin.tt_user_point_hdr.edit', compact('om_user'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['point'] = $data['point'];
		$saveData['remarks'] = $data['remarks'];

        $tt_user_point_hdr = ttUserPointHdr::create($saveData);

        return redirect('/admin/tt_user_point_hdr')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $tt_user_point_hdr = ttUserPointHdr::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();

        return view('admin.tt_user_point_hdr.edit', compact('tt_user_point_hdr','om_user'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['point'] = $data['point'];
		$saveData['remarks'] = $data['remarks'];

        $row = ttUserPointHdr::where('id', $id)->first();
        if ($row){
            $ttUserPointHdr = ttUserPointHdr::where('id', $id)->update($saveData);
        }

        return redirect('/admin/tt_user_point_hdr')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = ttUserPointHdr::where('id', $request->id)->delete();
        return redirect('/admin/tt_user_point_hdr');

    }
}

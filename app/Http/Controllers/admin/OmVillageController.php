<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmVillage;
use Illuminate\Support\Facades\DB;

//add models here

class OmVillageController extends Controller
{
    public function index(){
        $om_village = OmVillage::paginate(30);
        return view('admin.om_village.all', compact('om_village'));
    }

    public function create(){

        $om_block = DB::table('om_block')->get();

        return view('admin.om_village.edit', compact('om_block'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['village_name'] = $data['village_name'];
		$saveData['block_id'] = $data['block_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_village = OmVillage::create($saveData);

        return redirect('/admin/om_village')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_village = OmVillage::where('village_id', $id)->first();
        $om_block = DB::table('om_block')->get();

        return view('admin.om_village.edit', compact('om_village','om_block'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['village_name'] = $data['village_name'];
		$saveData['block_id'] = $data['block_id'];
		$saveData['pincode'] = $data['pincode'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmVillage::where('village_id', $id)->first();
        if ($row){
            $OmVillage = OmVillage::where('village_id', $id)->update($saveData);
        }

        return redirect('/admin/om_village')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmVillage::where('village_id', $request->id)->delete();
        return redirect('/admin/om_village');

    }
}

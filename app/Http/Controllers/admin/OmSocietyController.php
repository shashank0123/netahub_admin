<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmSociety;
use Illuminate\Support\Facades\DB;

//add models here

class OmSocietyController extends Controller
{
    public function index(){
        $om_society = OmSociety::paginate(30);
        return view('admin.om_society.all', compact('om_society'));
    }

    public function create(){

        $om_district = DB::table('om_district')->get();

        return view('admin.om_society.edit', compact('om_district'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['society_name'] = $data['society_name'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_society = OmSociety::create($saveData);

        return redirect('/admin/om_society')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_society = OmSociety::where('society_id', $id)->first();
        $om_district = DB::table('om_district')->get();

        return view('admin.om_society.edit', compact('om_society','om_district'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['society_name'] = $data['society_name'];
		$saveData['district_id'] = $data['district_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmSociety::where('society_id', $id)->first();
        if ($row){
            $OmSociety = OmSociety::where('society_id', $id)->update($saveData);
        }

        return redirect('/admin/om_society')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmSociety::where('society_id', $request->id)->delete();
        return redirect('/admin/om_society');

    }
}

<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmCollege;
use Illuminate\Support\Facades\DB;

//add models here

class OmCollegeController extends Controller
{
    public function index(){
        $om_college = OmCollege::paginate(30);
        return view('admin.om_college.all', compact('om_college'));
    }

    public function create(){

        $om_state = DB::table('om_state')->get();

        return view('admin.om_college.edit', compact('om_state'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['college_name'] = $data['college_name'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_college = OmCollege::create($saveData);

        return redirect('/admin/om_college')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_college = OmCollege::where('college_id', $id)->first();
        $om_state = DB::table('om_state')->get();

        return view('admin.om_college.edit', compact('om_college','om_state'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['college_name'] = $data['college_name'];
		$saveData['state_id'] = $data['state_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmCollege::where('college_id', $id)->first();
        if ($row){
            $OmCollege = OmCollege::where('college_id', $id)->update($saveData);
        }

        return redirect('/admin/om_college')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmCollege::where('college_id', $request->id)->delete();
        return redirect('/admin/om_college');

    }
}

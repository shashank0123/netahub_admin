<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmUserDevice;
use Illuminate\Support\Facades\DB;

//add models here

class OmUserDeviceController extends Controller
{
    public function index(){
        $om_user_device = OmUserDevice::paginate(30);
        return view('admin.om_user_device.all', compact('om_user_device'));
    }

    public function create(){

        $om_user = DB::table('om_user')->get();

        return view('admin.om_user_device.edit', compact('om_user'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['token_id'] = $data['token_id'];
		$saveData['imei'] = $data['imei'];
		$saveData['model'] = $data['model'];
		$saveData['status'] = $data['status'];

        $om_user_device = OmUserDevice::create($saveData);

        return redirect('/admin/om_user_device')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_user_device = OmUserDevice::where('id', $id)->first();
        $om_user = DB::table('om_user')->get();

        return view('admin.om_user_device.edit', compact('om_user_device','om_user'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['user_id'] = $data['user_id'];
		$saveData['token_id'] = $data['token_id'];
		$saveData['imei'] = $data['imei'];
		$saveData['model'] = $data['model'];
		$saveData['status'] = $data['status'];

        $row = OmUserDevice::where('id', $id)->first();
        if ($row){
            $OmUserDevice = OmUserDevice::where('id', $id)->update($saveData);
        }

        return redirect('/admin/om_user_device')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmUserDevice::where('id', $request->id)->delete();
        return redirect('/admin/om_user_device');

    }
}

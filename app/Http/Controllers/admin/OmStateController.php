<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\OmState;
use Illuminate\Support\Facades\DB;

//add models here

class OmStateController extends Controller
{
    public function index(){
        $om_state = OmState::paginate(30);
        return view('admin.om_state.all', compact('om_state'));
    }

    public function create(){

        $om_country = DB::table('om_country')->get();

        return view('admin.om_state.edit', compact('om_country'));
    }


    public function store(Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['state_name'] = $data['state_name'];
		$saveData['country_id'] = $data['country_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $om_state = OmState::create($saveData);

        return redirect('/admin/om_state')->with('successMsg','Data has been saved.');
    }


    public function edit($id){
        $om_state = OmState::where('state_id,', $id)->first();
        $om_country = DB::table('om_country')->get();

        return view('admin.om_state.edit', compact('om_state','om_country'));
    }


    public function update($id, Request $request){
        $data = request()->all();
        $saveData = [];
		$saveData['state_name'] = $data['state_name'];
		$saveData['country_id'] = $data['country_id'];
		$saveData['status'] = $data['status'];
		$saveData['ent_on'] = $data['ent_on'];
		$saveData['ent_by'] = $data['ent_by'];
		$saveData['auth_on'] = $data['auth_on'];
		$saveData['auth_by'] = $data['auth_by'];

        $row = OmState::where('state_id,', $id)->first();
        if ($row){
            $OmState = OmState::where('state_id,', $id)->update($saveData);
        }

        return redirect('/admin/om_state')->with('successMsg','Data has been saved.');
    }

    public function delete(Request $request)
    {
        $delete = OmState::where('state_id,', $request->id)->delete();
        return redirect('/admin/om_state');

    }
}

@extends('dashboard.layouts.dashboard')

@section('mainarea')
	
	<div class="card" style="width: 18rem;">
  <img src="{{ (!empty($user->profile_photo_path))? url('uploads/user_image/'.$user->profile_photo_path): url('/uploads/noimage.jpg') }}" class="card-img-top">
  <div class="card-body">
    <h5 class="card-title">User Name : {{$user->name}}</h5>
    <p class="card-text">Email : {{$user->email}}</p>
    <a href="{{route('user.profile.edit')}}" class="btn btn-primary">Edit Profile</a>
  </div>
</div>

@endsection
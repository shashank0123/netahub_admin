@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_college/{{isset($om_college) ? "edit/".$om_college->college_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">College name</label>
              <input type="text" name="college_name" value="{{isset($om_college->college_name) ? $om_college->college_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">State id</label>
                              <select class="form-control" name="state_id" aria-label="Default select example">
                                  <option selected>Select State id</option>
                                  @foreach ($om_state as $desc)
                                  <option {{(isset($om_college->state_id) && $desc->state_id == $om_college->state_id) ? "selected" : ""}} value="{{$desc->state_id}}">{{$desc->state_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_college->status) ? $om_college->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_college->ent_on) ? $om_college->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_college->ent_by) ? $om_college->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_college->auth_on) ? $om_college->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_college->auth_by) ? $om_college->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


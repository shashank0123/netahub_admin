@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>User id</td>
<td>Point</td>
<td>Remarks</td>

                <td>Actions</td>
                </tr>
                @foreach ($tt_user_point_hdr as $value)
                <tr>
                <td>{{$value->id}}</td>
                <?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_user')->where('user_id', $value->user_id)->first();


                        ?>
                        <td>{{isset($temp->user_id) ? $temp->user_id : $value->user_id}}</td>
<td>{{$value->point}}</td>
<td>{{$value->remarks}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/tt_user_point_hdr/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $tt_user_point_hdr->links() }}
        </div>
        @endsection
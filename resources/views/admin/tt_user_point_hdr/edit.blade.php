@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/tt_user_point_hdr/{{isset($tt_user_point_hdr) ? "edit/".$tt_user_point_hdr->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($tt_user_point_hdr->user_id) && $desc->user_id == $tt_user_point_hdr->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_id}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Point</label>
              <input type="text" name="point" value="{{isset($tt_user_point_hdr->point) ? $tt_user_point_hdr->point : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Remarks</label>
              <input type="text" name="remarks" value="{{isset($tt_user_point_hdr->remarks) ? $tt_user_point_hdr->remarks : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


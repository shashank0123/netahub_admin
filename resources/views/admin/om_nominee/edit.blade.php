@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_nominee/{{isset($om_nominee) ? "edit/".$om_nominee->nominee_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Nominee name</label>
              <input type="text" name="nominee_name" value="{{isset($om_nominee->nominee_name) ? $om_nominee->nominee_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Mobile no</label>
              <input type="text" name="mobile_no" value="{{isset($om_nominee->mobile_no) ? $om_nominee->mobile_no : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Nominee desc</label>
              <input type="text" name="nominee_desc" value="{{isset($om_nominee->nominee_desc) ? $om_nominee->nominee_desc : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($om_nominee->user_id) && $desc->user_id == $om_nominee->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


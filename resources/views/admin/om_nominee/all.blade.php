@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Nominee id</td>
                <td>Nominee name</td>
<td>Mobile no</td>
<td>Nominee desc</td>
<td>Photo path</td>
<td>User id</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_nominee as $value)
                <tr>
                <td>{{$value->nominee_id}}</td>
                <td>{{$value->nominee_name}}</td>
<td>{{$value->mobile_no}}</td>
<td>{{$value->nominee_desc}}</td>
<td>{{$value->photo_path}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_user')->where('user_id', $value->user_id)->first();


                        ?>
                        <td>{{isset($temp->user_name) ? $temp->user_name : $value->user_id}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_nominee/edit/{{$value->nominee_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_nominee->links() }}
        </div>
        @endsection
@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_tehsil/{{isset($om_tehsil) ? "edit/".$om_tehsil->tehsil_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Tehsil name</label>
              <input type="text" name="tehsil_name" value="{{isset($om_tehsil->tehsil_name) ? $om_tehsil->tehsil_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">District id</label>
                              <select class="form-control" name="district_id" aria-label="Default select example">
                                  <option selected>Select District id</option>
                                  @foreach ($om_district as $desc)
                                  <option {{(isset($om_tehsil->district_id) && $desc->district_id == $om_tehsil->district_id) ? "selected" : ""}} value="{{$desc->district_id}}">{{$desc->district_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_tehsil->status) ? $om_tehsil->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_tehsil->ent_on) ? $om_tehsil->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_tehsil->ent_by) ? $om_tehsil->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_tehsil->auth_on) ? $om_tehsil->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_tehsil->auth_by) ? $om_tehsil->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


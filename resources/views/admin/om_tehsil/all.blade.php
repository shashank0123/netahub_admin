@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Tehsil id,</td>
                <td>Tehsil name</td>
<td>District id</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_tehsil as $value)
                <tr>
                <td>{{$value->tehsil_id,}}</td>
                <td>{{$value->tehsil_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_district')->where('district_id', $value->district_id)->first();


                        ?>
                        <td>{{isset($temp->district_name) ? $temp->district_name : $value->district_id}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_tehsil/edit/{{$value->tehsil_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_tehsil->links() }}
        </div>
        @endsection
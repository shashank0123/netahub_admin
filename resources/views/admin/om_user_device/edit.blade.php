@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_user_device/{{isset($om_user_device) ? "edit/".$om_user_device->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($om_user_device->user_id) && $desc->user_id == $om_user_device->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Token id</label>
              <input type="text" name="token_id" value="{{isset($om_user_device->token_id) ? $om_user_device->token_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Imei</label>
              <input type="text" name="imei" value="{{isset($om_user_device->imei) ? $om_user_device->imei : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Model</label>
              <input type="text" name="model" value="{{isset($om_user_device->model) ? $om_user_device->model : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_user_device->status) ? $om_user_device->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


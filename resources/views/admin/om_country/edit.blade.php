@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_country/{{isset($om_country) ? "edit/".$om_country->country_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Country name</label>
              <input type="text" name="country_name" value="{{isset($om_country->country_name) ? $om_country->country_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_country->status) ? $om_country->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_country->ent_on) ? $om_country->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_country->ent_by) ? $om_country->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_country->auth_on) ? $om_country->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_country->auth_by) ? $om_country->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


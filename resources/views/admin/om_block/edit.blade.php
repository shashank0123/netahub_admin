@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_block/{{isset($om_block) ? "edit/".$om_block->block_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Block name</label>
              <input type="text" name="block_name" value="{{isset($om_block->block_name) ? $om_block->block_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Tehsil id</label>
                              <select class="form-control" name="tehsil_id" aria-label="Default select example">
                                  <option selected>Select Tehsil id</option>
                                  @foreach ($om_tehsil as $desc)
                                  <option {{(isset($om_block->tehsil_id) && $desc->tehsil_id == $om_block->tehsil_id) ? "selected" : ""}} value="{{$desc->tehsil_id}}">{{$desc->tehsil_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Block type</label>
                              <select class="form-control" name="block_type" aria-label="Default select example">
                                  <option selected>Select Block type</option><option {{(isset($om_block->block_type) && 'N' == $om_block->block_type) ? 'selected' : ''}} value='N'>Nagar Nigam</option>
                                  <option {{(isset($om_block->block_type) && 'B' == $om_block->block_type) ? 'selected' : ''}} value='B'>Block</option>
                                  </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_block->status) ? $om_block->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="date" name="ent_on" value="{{isset($om_block->ent_on) ? $om_block->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_block->ent_by) ? $om_block->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="date" name="auth_on" value="{{isset($om_block->auth_on) ? $om_block->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_block->auth_by) ? $om_block->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


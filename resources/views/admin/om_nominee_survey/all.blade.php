@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>Survey id</td>
<td>Cons id</td>
<td>Nominee id</td>
<td>Party id</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_nominee_survey as $value)
                <tr>
                <td>{{$value->id}}</td>
                <?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_survey')->where('survey_id', $value->survey_id)->first();


                        ?>
                        <td>{{isset($temp->survey_name) ? $temp->survey_name : $value->survey_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_constituency')->where('cons_id', $value->cons_id)->first();


                        ?>
                        <td>{{isset($temp->cons_name) ? $temp->cons_name : $value->cons_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_nominee')->where('nominee_id', $value->nominee_id)->first();


                        ?>
                        <td>{{isset($temp->nominee_name) ? $temp->nominee_name : $value->nominee_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_party')->where('party_id', $value->party_id)->first();


                        ?>
                        <td>{{isset($temp->party_name) ? $temp->party_name : $value->party_id}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_nominee_survey/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_nominee_survey->links() }}
        </div>
        @endsection
@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_nominee_survey/{{isset($om_nominee_survey) ? "edit/".$om_nominee_survey->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">Survey id</label>
                              <select class="form-control" name="survey_id" aria-label="Default select example">
                                  <option selected>Select Survey id</option>
                                  @foreach ($om_survey as $desc)
                                  <option {{(isset($om_nominee_survey->survey_id) && $desc->survey_id == $om_nominee_survey->survey_id) ? "selected" : ""}} value="{{$desc->survey_id}}">{{$desc->survey_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Cons id</label>
                              <select class="form-control" name="cons_id" aria-label="Default select example">
                                  <option selected>Select Cons id</option>
                                  @foreach ($om_constituency as $desc)
                                  <option {{(isset($om_nominee_survey->cons_id) && $desc->cons_id == $om_nominee_survey->cons_id) ? "selected" : ""}} value="{{$desc->cons_id}}">{{$desc->cons_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Nominee id</label>
                              <select class="form-control" name="nominee_id" aria-label="Default select example">
                                  <option selected>Select Nominee id</option>
                                  @foreach ($om_nominee as $desc)
                                  <option {{(isset($om_nominee_survey->nominee_id) && $desc->nominee_id == $om_nominee_survey->nominee_id) ? "selected" : ""}} value="{{$desc->nominee_id}}">{{$desc->nominee_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Party id</label>
                              <select class="form-control" name="party_id" aria-label="Default select example">
                                  <option selected>Select Party id</option>
                                  @foreach ($om_party as $desc)
                                  <option {{(isset($om_nominee_survey->party_id) && $desc->party_id == $om_nominee_survey->party_id) ? "selected" : ""}} value="{{$desc->party_id}}">{{$desc->party_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


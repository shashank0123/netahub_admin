@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Cons id</td>
                <td>Cons name</td>
<td>Election id</td>
<td>Ref id</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_constituency as $value)
                <tr>
                <td>{{$value->cons_id}}</td>
                <td>{{$value->cons_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_election')->where('election_id', $value->election_id)->first();


                        ?>
                        <td>{{isset($temp->election_name) ? $temp->election_name : $value->election_id}}</td>
<td>{{$value->ref_id}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_constituency/edit/{{$value->cons_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_constituency->links() }}
        </div>
        @endsection
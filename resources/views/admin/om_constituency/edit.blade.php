@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_constituency/{{isset($om_constituency) ? "edit/".$om_constituency->cons_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Cons name</label>
              <input type="text" name="cons_name" value="{{isset($om_constituency->cons_name) ? $om_constituency->cons_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Election id</label>
                              <select class="form-control" name="election_id" aria-label="Default select example">
                                  <option selected>Select Election id</option>
                                  @foreach ($om_election as $desc)
                                  <option {{(isset($om_constituency->election_id) && $desc->election_id == $om_constituency->election_id) ? "selected" : ""}} value="{{$desc->election_id}}">{{$desc->election_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ref id</label>
              <input type="text" name="ref_id" value="{{isset($om_constituency->ref_id) ? $om_constituency->ref_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>Param id</td>
<td> parent id</td>
<td> param type</td>
<td> content type</td>
<td> content detail</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_param_contents as $value)
                <tr>
                <td>{{$value->id}}</td>
                <?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_state')->where('param_id', $value->param_id)->first();


                        ?>
                        <td>{{isset($temp->state_name) ? $temp->state_name : $value->param_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_country')->where(' parent_id', $value-> parent_id)->first();


                        ?>
                        <td>{{isset($temp->country_name) ? $temp->country_name : $value-> parent_id}}</td>
<td>{{$value-> param_type}}</td>
<td>{{$value-> content_type}}</td>
<td>{{$value-> content_detail}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_param_contents/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_param_contents->links() }}
        </div>
        @endsection
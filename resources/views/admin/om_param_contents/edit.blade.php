@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_param_contents/{{isset($om_param_contents) ? "edit/".$om_param_contents->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">Param id</label>
                              <select class="form-control" name="param_id" aria-label="Default select example">
                                  <option selected>Select Param id</option>
                                  @foreach ($om_state as $desc)
                                  <option {{(isset($om_param_contents->param_id) && $desc->param_id == $om_param_contents->param_id) ? "selected" : ""}} value="{{$desc->param_id}}">{{$desc->state_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label"> parent id</label>
                              <select class="form-control" name=" parent_id" aria-label="Default select example">
                                  <option selected>Select  parent id</option>
                                  @foreach ($om_country as $desc)
                                  <option {{(isset($om_param_contents-> parent_id) && $desc-> parent_id == $om_param_contents-> parent_id) ? "selected" : ""}} value="{{$desc-> parent_id}}">{{$desc->country_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label"> param type</label>
                              <select class="form-control" name=" param_type" aria-label="Default select example">
                                  <option selected>Select  param type</option><option {{(isset($om_param_contents-> param_type) && 'S' == $om_param_contents-> param_type) ? 'selected' : ''}} value='S'>STATE</option>
                                  </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label"> content type</label>
                              <select class="form-control" name=" content_type" aria-label="Default select example">
                                  <option selected>Select  content type</option><option {{(isset($om_param_contents-> content_type) && 'R' == $om_param_contents-> content_type) ? 'selected' : ''}} value='R'>REN_CONTENT</option>
                                  </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_state/{{isset($om_state) ? "edit/".$om_state->state_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">State name</label>
              <input type="text" name="state_name" value="{{isset($om_state->state_name) ? $om_state->state_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Country id</label>
                              <select class="form-control" name="country_id" aria-label="Default select example">
                                  <option selected>Select Country id</option>
                                  @foreach ($om_country as $desc)
                                  <option {{(isset($om_state->country_id) && $desc->country_id == $om_state->country_id) ? "selected" : ""}} value="{{$desc->country_id}}">{{$desc->country_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_state->status) ? $om_state->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_state->ent_on) ? $om_state->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_state->ent_by) ? $om_state->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_state->auth_on) ? $om_state->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_state->auth_by) ? $om_state->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


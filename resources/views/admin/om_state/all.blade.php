@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>State id,</td>
                <td>State name</td>
<td>Country id</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_state as $value)
                <tr>
                <td>{{$value->state_id,}}</td>
                <td>{{$value->state_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_country')->where('country_id', $value->country_id)->first();


                        ?>
                        <td>{{isset($temp->country_name) ? $temp->country_name : $value->country_id}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_state/edit/{{$value->state_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_state->links() }}
        </div>
        @endsection
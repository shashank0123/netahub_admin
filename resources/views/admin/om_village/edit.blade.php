@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_village/{{isset($om_village) ? "edit/".$om_village->village_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Village name</label>
              <input type="text" name="village_name" value="{{isset($om_village->village_name) ? $om_village->village_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Block id</label>
                              <select class="form-control" name="block_id" aria-label="Default select example">
                                  <option selected>Select Block id</option>
                                  @foreach ($om_block as $desc)
                                  <option {{(isset($om_village->block_id) && $desc->block_id == $om_village->block_id) ? "selected" : ""}} value="{{$desc->block_id}}">{{$desc->block_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Pincode</label>
              <input type="text" name="pincode" value="{{isset($om_village->pincode) ? $om_village->pincode : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_village->status) ? $om_village->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_village->ent_on) ? $om_village->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_village->ent_by) ? $om_village->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_village->auth_on) ? $om_village->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_village->auth_by) ? $om_village->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Village id</td>
                <td>Village name</td>
<td>Block id</td>
<td>Pincode</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_village as $value)
                <tr>
                <td>{{$value->village_id}}</td>
                <td>{{$value->village_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_block')->where('block_id', $value->block_id)->first();


                        ?>
                        <td>{{isset($temp->block_name) ? $temp->block_name : $value->block_id}}</td>
<td>{{$value->pincode}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_village/edit/{{$value->village_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_village->links() }}
        </div>
        @endsection
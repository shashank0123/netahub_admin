@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>District id</td>
                <td>District name</td>
<td>State id</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_district as $value)
                <tr>
                <td>{{$value->district_id}}</td>
                <td>{{$value->district_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_state')->where('state_id', $value->state_id)->first();


                        ?>
                        <td>{{isset($temp->state_name) ? $temp->state_name : $value->state_id}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_district/edit/{{$value->district_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_district->links() }}
        </div>
        @endsection
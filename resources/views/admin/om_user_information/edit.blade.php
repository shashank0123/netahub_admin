@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_user_information/{{isset($om_user_information) ? "edit/".$om_user_information->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($om_user_information->user_id) && $desc->user_id == $om_user_information->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">User information</label>
              <input type="text" name="user_information" value="{{isset($om_user_information->user_information) ? $om_user_information->user_information : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


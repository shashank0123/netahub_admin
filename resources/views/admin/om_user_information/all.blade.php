@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>User id</td>
<td>User information</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_user_information as $value)
                <tr>
                <td>{{$value->id}}</td>
                <?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_user')->where('user_id', $value->user_id)->first();


                        ?>
                        <td>{{isset($temp->user_name) ? $temp->user_name : $value->user_id}}</td>
<td>{{$value->user_information}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_user_information/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_user_information->links() }}
        </div>
        @endsection
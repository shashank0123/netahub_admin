@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/tt_notification/{{isset($tt_notification) ? "edit/".$tt_notification->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Title</label>
              <input type="text" name="title" value="{{isset($tt_notification->title) ? $tt_notification->title : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Message</label>
              <input type="text" name="message" value="{{isset($tt_notification->message) ? $tt_notification->message : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Tone</label>
                              <select class="form-control" name="tone" aria-label="Default select example">
                                  <option selected>Select Tone</option><option {{(isset($tt_notification->tone) && 'd' == $tt_notification->tone) ? 'selected' : ''}} value='d'>default</option>
                                  </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($tt_notification->user_id) && $desc->user_id == $tt_notification->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Location type</label>
              <input type="text" name="location_type" value="{{isset($tt_notification->location_type) ? $tt_notification->location_type : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Location id</label>
              <input type="text" name="location_id" value="{{isset($tt_notification->location_id) ? $tt_notification->location_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Cons id</label>
                              <select class="form-control" name="cons_id" aria-label="Default select example">
                                  <option selected>Select Cons id</option>
                                  @foreach ($om_constituency as $desc)
                                  <option {{(isset($tt_notification->cons_id) && $desc->cons_id == $tt_notification->cons_id) ? "selected" : ""}} value="{{$desc->cons_id}}">{{$desc->cons_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Status</label>
                              <select class="form-control" name="status" aria-label="Default select example">
                                  <option selected>Select Status</option><option {{(isset($tt_notification->status) && 'e' == $tt_notification->status) ? 'selected' : ''}} value='e'>enable</option>
                                  </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>Title</td>
<td>Message</td>
<td>Photo path</td>
<td>Tone</td>
<td>User id</td>
<td>Location type</td>
<td>Location id</td>
<td>Cons id</td>
<td>Status</td>

                <td>Actions</td>
                </tr>
                @foreach ($tt_notification as $value)
                <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->title}}</td>
<td>{{$value->message}}</td>
<td>{{$value->photo_path}}</td>
<td>{{$value->tone}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_user')->where('user_id', $value->user_id)->first();


                        ?>
                        <td>{{isset($temp->user_name) ? $temp->user_name : $value->user_id}}</td>
<td>{{$value->location_type}}</td>
<td>{{$value->location_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_constituency')->where('cons_id', $value->cons_id)->first();


                        ?>
                        <td>{{isset($temp->cons_name) ? $temp->cons_name : $value->cons_id}}</td>
<td>{{$value->status}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/tt_notification/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $tt_notification->links() }}
        </div>
        @endsection
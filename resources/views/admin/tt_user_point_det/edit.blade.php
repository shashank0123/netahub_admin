@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/tt_user_point_det/{{isset($tt_user_point_det) ? "edit/".$tt_user_point_det->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">User id</label>
              <input type="text" name="user_id" value="{{isset($tt_user_point_det->user_id) ? $tt_user_point_det->user_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Point</label>
              <input type="text" name="point" value="{{isset($tt_user_point_det->point) ? $tt_user_point_det->point : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Remarks</label>
              <input type="text" name="remarks" value="{{isset($tt_user_point_det->remarks) ? $tt_user_point_det->remarks : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


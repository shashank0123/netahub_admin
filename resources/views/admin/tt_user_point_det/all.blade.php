@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>User id</td>
<td>Log date</td>
<td>Trns type</td>
<td>Point</td>
<td>Remarks</td>

                <td>Actions</td>
                </tr>
                @foreach ($tt_user_point_det as $value)
                <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->user_id}}</td>
<td>{{$value->log_date}}</td>
<td>{{$value->trns_type}}</td>
<td>{{$value->point}}</td>
<td>{{$value->remarks}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/tt_user_point_det/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $tt_user_point_det->links() }}
        </div>
        @endsection
@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_user_cons_map/{{isset($om_user_cons_map) ? "edit/".$om_user_cons_map->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($om_user_cons_map->user_id) && $desc->user_id == $om_user_cons_map->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Cons id</label>
                              <select class="form-control" name="cons_id" aria-label="Default select example">
                                  <option selected>Select Cons id</option>
                                  @foreach ($om_constituency as $desc)
                                  <option {{(isset($om_user_cons_map->cons_id) && $desc->cons_id == $om_user_cons_map->cons_id) ? "selected" : ""}} value="{{$desc->cons_id}}">{{$desc->cons_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Election id</label>
                              <select class="form-control" name="election_id" aria-label="Default select example">
                                  <option selected>Select Election id</option>
                                  @foreach ($om_election as $desc)
                                  <option {{(isset($om_user_cons_map->election_id) && $desc->election_id == $om_user_cons_map->election_id) ? "selected" : ""}} value="{{$desc->election_id}}">{{$desc->election_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


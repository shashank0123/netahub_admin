@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Id</td>
                <td>User id</td>
<td>Cons id</td>
<td>Election id</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_user_cons_map as $value)
                <tr>
                <td>{{$value->id}}</td>
                <?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_user')->where('user_id', $value->user_id)->first();


                        ?>
                        <td>{{isset($temp->user_name) ? $temp->user_name : $value->user_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_constituency')->where('cons_id', $value->cons_id)->first();


                        ?>
                        <td>{{isset($temp->cons_name) ? $temp->cons_name : $value->cons_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_election')->where('election_id', $value->election_id)->first();


                        ?>
                        <td>{{isset($temp->election_name) ? $temp->election_name : $value->election_id}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_user_cons_map/edit/{{$value->id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_user_cons_map->links() }}
        </div>
        @endsection
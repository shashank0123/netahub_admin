@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Party id,</td>
                <td>Party name</td>
<td>Party code</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_party as $value)
                <tr>
                <td>{{$value->party_id,}}</td>
                <td>{{$value->party_name}}</td>
<td>{{$value->party_code}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_party/edit/{{$value->party_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_party->links() }}
        </div>
        @endsection
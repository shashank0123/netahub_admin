@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_party/{{isset($om_party) ? "edit/".$om_party->party_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Party name</label>
              <input type="text" name="party_name" value="{{isset($om_party->party_name) ? $om_party->party_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Party code</label>
              <input type="text" name="party_code" value="{{isset($om_party->party_code) ? $om_party->party_code : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_survey/{{isset($om_survey) ? "edit/".$om_survey->survey_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Survey name</label>
              <input type="text" name="survey_name" value="{{isset($om_survey->survey_name) ? $om_survey->survey_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Election id</label>
                              <select class="form-control" name="election_id" aria-label="Default select example">
                                  <option selected>Select Election id</option>
                                  @foreach ($om_election as $desc)
                                  <option {{(isset($om_survey->election_id) && $desc->election_id == $om_survey->election_id) ? "selected" : ""}} value="{{$desc->election_id}}">{{$desc->election_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


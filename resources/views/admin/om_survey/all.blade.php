@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Survey id,</td>
                <td>Survey name</td>
<td>From date</td>
<td>To date</td>
<td>Election id</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_survey as $value)
                <tr>
                <td>{{$value->survey_id,}}</td>
                <td>{{$value->survey_name}}</td>
<td>{{$value->from_date}}</td>
<td>{{$value->to_date}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_election')->where('election_id', $value->election_id)->first();


                        ?>
                        <td>{{isset($temp->election_name) ? $temp->election_name : $value->election_id}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_survey/edit/{{$value->survey_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_survey->links() }}
        </div>
        @endsection
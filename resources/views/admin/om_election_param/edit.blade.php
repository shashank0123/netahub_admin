@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_election_param/{{isset($om_election_param) ? "edit/".$om_election_param->param_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Param code</label>
              <input type="text" name="param_code" value="{{isset($om_election_param->param_code) ? $om_election_param->param_code : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Param desc</label>
              <input type="text" name="param_desc" value="{{isset($om_election_param->param_desc) ? $om_election_param->param_desc : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Param id</td>
                <td>Param code</td>
<td>Param desc</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_election_param as $value)
                <tr>
                <td>{{$value->param_id}}</td>
                <td>{{$value->param_code}}</td>
<td>{{$value->param_desc}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_election_param/edit/{{$value->param_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_election_param->links() }}
        </div>
        @endsection
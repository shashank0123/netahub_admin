@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_survey_cons_map/{{isset($om_survey_cons_map) ? "edit/".$om_survey_cons_map->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">Survey id</label>
                              <select class="form-control" name="survey_id" aria-label="Default select example">
                                  <option selected>Select Survey id</option>
                                  @foreach ($om_survey as $desc)
                                  <option {{(isset($om_survey_cons_map->survey_id) && $desc->survey_id == $om_survey_cons_map->survey_id) ? "selected" : ""}} value="{{$desc->survey_id}}">{{$desc->survey_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Cons id</label>
                              <select class="form-control" name="cons_id" aria-label="Default select example">
                                  <option selected>Select Cons id</option>
                                  @foreach ($om_constituency as $desc)
                                  <option {{(isset($om_survey_cons_map->cons_id) && $desc->cons_id == $om_survey_cons_map->cons_id) ? "selected" : ""}} value="{{$desc->cons_id}}">{{$desc->cons_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


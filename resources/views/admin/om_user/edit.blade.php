@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_user/{{isset($om_user) ? "edit/".$om_user->user_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">User name</label>
              <input type="text" name="user_name" value="{{isset($om_user->user_name) ? $om_user->user_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Display name</label>
              <input type="text" name="display_name" value="{{isset($om_user->display_name) ? $om_user->display_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">User password</label>
              <input type="text" name="user_password" value="{{isset($om_user->user_password) ? $om_user->user_password : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Email id</label>
              <input type="text" name="email_id" value="{{isset($om_user->email_id) ? $om_user->email_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Mobile no</label>
              <input type="text" name="mobile_no" value="{{isset($om_user->mobile_no) ? $om_user->mobile_no : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Qualification</label>
              <input type="text" name="qualification" value="{{isset($om_user->qualification) ? $om_user->qualification : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Occupation</label>
              <input type="text" name="occupation" value="{{isset($om_user->occupation) ? $om_user->occupation : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Gender</label>
              <input type="text" name="gender" value="{{isset($om_user->gender) ? $om_user->gender : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">College id</label>
                              <select class="form-control" name="college_id" aria-label="Default select example">
                                  <option selected>Select College id</option>
                                  @foreach ($om_college as $desc)
                                  <option {{(isset($om_user->college_id) && $desc->college_id == $om_user->college_id) ? "selected" : ""}} value="{{$desc->college_id}}">{{$desc->college_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Society id</label>
                              <select class="form-control" name="society_id" aria-label="Default select example">
                                  <option selected>Select Society id</option>
                                  @foreach ($om_society as $desc)
                                  <option {{(isset($om_user->society_id) && $desc->society_id == $om_user->society_id) ? "selected" : ""}} value="{{$desc->society_id}}">{{$desc->society_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Ward id</label>
                              <select class="form-control" name="ward_id" aria-label="Default select example">
                                  <option selected>Select Ward id</option>
                                  @foreach ($om_ward as $desc)
                                  <option {{(isset($om_user->ward_id) && $desc->ward_id == $om_user->ward_id) ? "selected" : ""}} value="{{$desc->ward_id}}">{{$desc->ward_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Village id</label>
                              <select class="form-control" name="village_id" aria-label="Default select example">
                                  <option selected>Select Village id</option>
                                  @foreach ($om_village as $desc)
                                  <option {{(isset($om_user->village_id) && $desc->village_id == $om_user->village_id) ? "selected" : ""}} value="{{$desc->village_id}}">{{$desc->village_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Block id</label>
                              <select class="form-control" name="block_id" aria-label="Default select example">
                                  <option selected>Select Block id</option>
                                  @foreach ($om_block as $desc)
                                  <option {{(isset($om_user->block_id) && $desc->block_id == $om_user->block_id) ? "selected" : ""}} value="{{$desc->block_id}}">{{$desc->block_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Tehsil id</label>
                              <select class="form-control" name="tehsil_id" aria-label="Default select example">
                                  <option selected>Select Tehsil id</option>
                                  @foreach ($om_tehsil as $desc)
                                  <option {{(isset($om_user->tehsil_id) && $desc->tehsil_id == $om_user->tehsil_id) ? "selected" : ""}} value="{{$desc->tehsil_id}}">{{$desc->tehsil_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">District id</label>
                              <select class="form-control" name="district_id" aria-label="Default select example">
                                  <option selected>Select District id</option>
                                  @foreach ($om_district as $desc)
                                  <option {{(isset($om_user->district_id) && $desc->district_id == $om_user->district_id) ? "selected" : ""}} value="{{$desc->district_id}}">{{$desc->district_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">State id</label>
                              <select class="form-control" name="state_id" aria-label="Default select example">
                                  <option selected>Select State id</option>
                                  @foreach ($om_state as $desc)
                                  <option {{(isset($om_user->state_id) && $desc->state_id == $om_user->state_id) ? "selected" : ""}} value="{{$desc->state_id}}">{{$desc->state_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Country id</label>
                              <select class="form-control" name="country_id" aria-label="Default select example">
                                  <option selected>Select Country id</option>
                                  @foreach ($om_country as $desc)
                                  <option {{(isset($om_user->country_id) && $desc->country_id == $om_user->country_id) ? "selected" : ""}} value="{{$desc->country_id}}">{{$desc->country_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Token id</label>
              <input type="text" name="token_id" value="{{isset($om_user->token_id) ? $om_user->token_id : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Pincode</label>
              <input type="text" name="pincode" value="{{isset($om_user->pincode) ? $om_user->pincode : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_user->status) ? $om_user->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_user->ent_on) ? $om_user->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_user->ent_by) ? $om_user->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_user->auth_on) ? $om_user->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_user->auth_by) ? $om_user->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


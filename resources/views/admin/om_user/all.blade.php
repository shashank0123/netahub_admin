@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>User id,</td>
                <td>User name</td>
<td>Display name</td>
<td>User password</td>
<td>Email id</td>
<td>Mobile no</td>
<td>Dateofbirth</td>
<td>Qualification</td>
<td>Occupation</td>
<td>Gender</td>
<td>Photo path</td>
<td>College id</td>
<td>Society id</td>
<td>Ward id</td>
<td>Village id</td>
<td>Block id</td>
<td>Tehsil id</td>
<td>District id</td>
<td>State id</td>
<td>Country id</td>
<td>Token id</td>
<td>Pincode</td>
<td>Val cons</td>
<td>Referred by</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_user as $value)
                <tr>
                <td>{{$value->user_id,}}</td>
                <td>{{$value->user_name}}</td>
<td>{{$value->display_name}}</td>
<td>{{$value->user_password}}</td>
<td>{{$value->email_id}}</td>
<td>{{$value->mobile_no}}</td>
<td>{{$value->dateofbirth}}</td>
<td>{{$value->qualification}}</td>
<td>{{$value->occupation}}</td>
<td>{{$value->gender}}</td>
<td>{{$value->photo_path}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_college')->where('college_id', $value->college_id)->first();


                        ?>
                        <td>{{isset($temp->college_name) ? $temp->college_name : $value->college_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_society')->where('society_id', $value->society_id)->first();


                        ?>
                        <td>{{isset($temp->society_name) ? $temp->society_name : $value->society_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_ward')->where('ward_id', $value->ward_id)->first();


                        ?>
                        <td>{{isset($temp->ward_name) ? $temp->ward_name : $value->ward_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_village')->where('village_id', $value->village_id)->first();


                        ?>
                        <td>{{isset($temp->village_name) ? $temp->village_name : $value->village_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_block')->where('block_id', $value->block_id)->first();


                        ?>
                        <td>{{isset($temp->block_name) ? $temp->block_name : $value->block_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_tehsil')->where('tehsil_id', $value->tehsil_id)->first();


                        ?>
                        <td>{{isset($temp->tehsil_name) ? $temp->tehsil_name : $value->tehsil_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_district')->where('district_id', $value->district_id)->first();


                        ?>
                        <td>{{isset($temp->district_name) ? $temp->district_name : $value->district_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_state')->where('state_id', $value->state_id)->first();


                        ?>
                        <td>{{isset($temp->state_name) ? $temp->state_name : $value->state_id}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_country')->where('country_id', $value->country_id)->first();


                        ?>
                        <td>{{isset($temp->country_name) ? $temp->country_name : $value->country_id}}</td>
<td>{{$value->token_id}}</td>
<td>{{$value->pincode}}</td>
<td>{{$value->val_cons}}</td>
<td>{{$value->referred_by}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_user/edit/{{$value->user_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_user->links() }}
        </div>
        @endsection
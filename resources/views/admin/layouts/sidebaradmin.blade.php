<!-- ########## START: LEFT PANEL ########## -->
    <div class="sl-logo"><a href=""><i class="icon ion-android-star-outline"></i> starlight</a></div>
    <div class="sl-sideleft">
      <div class="input-group input-group-search">
        <input type="search" name="search" class="form-control" placeholder="Search">
        <span class="input-group-btn">
          <button class="btn"><i class="fa fa-search"></i></button>
        </span><!-- input-group-btn -->
      </div><!-- input-group -->

      <label class="sidebar-label">Navigation</label>
      <div class="sl-sideleft-menu">
        <a href="index.html" class="sl-menu-link active">
          <div class="sl-menu-item">
            <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
            <span class="menu-item-label">Dashboard</span>
          </div><!-- menu-item -->
        </a><!-- sl-menu-link -->

        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Block</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_block/add" class="nav-link">Add Om Block</a></li>
            <li class="nav-item"><a href="/admin/om_block" class="nav-link">View Om Block</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om College</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_college/add" class="nav-link">Add Om College</a></li>
            <li class="nav-item"><a href="/admin/om_college" class="nav-link">View Om College</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Constituency</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_constituency/add" class="nav-link">Add Om Constituency</a></li>
            <li class="nav-item"><a href="/admin/om_constituency" class="nav-link">View Om Constituency</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Country</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_country/add" class="nav-link">Add Om Country</a></li>
            <li class="nav-item"><a href="/admin/om_country" class="nav-link">View Om Country</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om District</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_district/add" class="nav-link">Add Om District</a></li>
            <li class="nav-item"><a href="/admin/om_district" class="nav-link">View Om District</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Election</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_election/add" class="nav-link">Add Om Election</a></li>
            <li class="nav-item"><a href="/admin/om_election" class="nav-link">View Om Election</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Election Param</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_election_param/add" class="nav-link">Add Om Election Param</a></li>
            <li class="nav-item"><a href="/admin/om_election_param" class="nav-link">View Om Election Param</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Faq</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_faq/add" class="nav-link">Add Om Faq</a></li>
            <li class="nav-item"><a href="/admin/om_faq" class="nav-link">View Om Faq</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Nominee</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_nominee/add" class="nav-link">Add Om Nominee</a></li>
            <li class="nav-item"><a href="/admin/om_nominee" class="nav-link">View Om Nominee</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Nominee Survey</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_nominee_survey/add" class="nav-link">Add Om Nominee Survey</a></li>
            <li class="nav-item"><a href="/admin/om_nominee_survey" class="nav-link">View Om Nominee Survey</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Party</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_party/add" class="nav-link">Add Om Party</a></li>
            <li class="nav-item"><a href="/admin/om_party" class="nav-link">View Om Party</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Society</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_society/add" class="nav-link">Add Om Society</a></li>
            <li class="nav-item"><a href="/admin/om_society" class="nav-link">View Om Society</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om State</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_state/add" class="nav-link">Add Om State</a></li>
            <li class="nav-item"><a href="/admin/om_state" class="nav-link">View Om State</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Survey</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_survey/add" class="nav-link">Add Om Survey</a></li>
            <li class="nav-item"><a href="/admin/om_survey" class="nav-link">View Om Survey</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Survey Cons Map</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_survey_cons_map/add" class="nav-link">Add Om Survey Cons Map</a></li>
            <li class="nav-item"><a href="/admin/om_survey_cons_map" class="nav-link">View Om Survey Cons Map</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Tehsil</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_tehsil/add" class="nav-link">Add Om Tehsil</a></li>
            <li class="nav-item"><a href="/admin/om_tehsil" class="nav-link">View Om Tehsil</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om User</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_user/add" class="nav-link">Add Om User</a></li>
            <li class="nav-item"><a href="/admin/om_user" class="nav-link">View Om User</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om User Cons Map</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_user_cons_map/add" class="nav-link">Add Om User Cons Map</a></li>
            <li class="nav-item"><a href="/admin/om_user_cons_map" class="nav-link">View Om User Cons Map</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om User Device</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_user_device/add" class="nav-link">Add Om User Device</a></li>
            <li class="nav-item"><a href="/admin/om_user_device" class="nav-link">View Om User Device</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om User information</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_user_information/add" class="nav-link">Add Om User information</a></li>
            <li class="nav-item"><a href="/admin/om_user_information" class="nav-link">View Om User information</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Village</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_village/add" class="nav-link">Add Om Village</a></li>
            <li class="nav-item"><a href="/admin/om_village" class="nav-link">View Om Village</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Ward</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_ward/add" class="nav-link">Add Om Ward</a></li>
            <li class="nav-item"><a href="/admin/om_ward" class="nav-link">View Om Ward</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Tt Feedback</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/tt_feedback/add" class="nav-link">Add Tt Feedback</a></li>
            <li class="nav-item"><a href="/admin/tt_feedback" class="nav-link">View Tt Feedback</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Tt Notification</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/tt_notification/add" class="nav-link">Add Tt Notification</a></li>
            <li class="nav-item"><a href="/admin/tt_notification" class="nav-link">View Tt Notification</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Tt User Point Det</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/tt_user_point_det/add" class="nav-link">Add Tt User Point Det</a></li>
            <li class="nav-item"><a href="/admin/tt_user_point_det" class="nav-link">View Tt User Point Det</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">tt User Point Hdr</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/tt_user_point_hdr/add" class="nav-link">Add tt User Point Hdr</a></li>
            <li class="nav-item"><a href="/admin/tt_user_point_hdr" class="nav-link">View tt User Point Hdr</a>
            </li>
          </ul>
          <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Om Param Contents</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="/admin/om_param_contents/add" class="nav-link">Add Om Param Contents</a></li>
            <li class="nav-item"><a href="/admin/om_param_contents" class="nav-link">View Om Param Contents</a>
            </li>
          </ul>
          {{-- add elements here --}}





        <a href="#" class="sl-menu-link">
            <div class="sl-menu-item">
              <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
              <span class="menu-item-label">Charts</span>
              <i class="menu-item-arrow fa fa-angle-down"></i>
            </div><!-- menu-item -->
          </a><!-- sl-menu-link -->
          <ul class="sl-menu-sub nav flex-column">
            <li class="nav-item"><a href="chart-morris.html" class="nav-link">Morris Charts</a></li>
            <li class="nav-item"><a href="chart-flot.html" class="nav-link">Flot Charts</a></li>
            <li class="nav-item"><a href="chart-chartjs.html" class="nav-link">Chart JS</a></li>
            <li class="nav-item"><a href="chart-rickshaw.html" class="nav-link">Rickshaw</a></li>
            <li class="nav-item"><a href="chart-sparkline.html" class="nav-link">Sparkline</a></li>
          </ul>
      </div><!-- sl-sideleft-menu -->

      <br>
    </div><!-- sl-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

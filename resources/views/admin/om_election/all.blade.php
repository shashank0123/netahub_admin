@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Election id</td>
                <td>Election name</td>
<td>Election image</td>
<td>Election description</td>
<td>Election type</td>
<td>Location type</td>
<td>Type</td>
<td>Req point</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_election as $value)
                <tr>
                <td>{{$value->election_id}}</td>
                <td>{{$value->election_name}}</td>
<td>{{$value->election_image}}</td>
<td>{{$value->election_description}}</td>
<td>{{$value->election_type}}</td>
<td>{{$value->location_type}}</td>
<td>{{$value->type}}</td>
<td>{{$value->req_point}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_election/edit/{{$value->election_id}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_election->links() }}
        </div>
        @endsection
@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_election/{{isset($om_election) ? "edit/".$om_election->election_id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Election name</label>
              <input type="text" name="election_name" value="{{isset($om_election->election_name) ? $om_election->election_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Election image</label>
              <input type="text" name="election_image" value="{{isset($om_election->election_image) ? $om_election->election_image : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Election description</label>
              <input type="text" name="election_description" value="{{isset($om_election->election_description) ? $om_election->election_description : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Election type</label>
                              <select class="form-control" name="election_type" aria-label="Default select example">
                                  <option selected>Select Election type</option><option {{(isset($om_election->election_type) && 'I' == $om_election->election_type) ? 'selected' : ''}} value='I'>INTERNAL</option>
                                  <option {{(isset($om_election->election_type) && 'E' == $om_election->election_type) ? 'selected' : ''}} value='E'>EXTERNAL</option>
                                  </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Location type</label>
                              <select class="form-control" name="location_type" aria-label="Default select example">
                                  <option selected>Select Location type</option><option {{(isset($om_election->location_type) && 'C' == $om_election->location_type) ? 'selected' : ''}} value='C'>COUNTRY</option>
                                  <option {{(isset($om_election->location_type) && 'S' == $om_election->location_type) ? 'selected' : ''}} value='S'>STATE</option>
                                  <option {{(isset($om_election->location_type) && 'D' == $om_election->location_type) ? 'selected' : ''}} value='D'>DISTRICT</option>
                                  <option {{(isset($om_election->location_type) && ' ' == $om_election->location_type) ? 'selected' : ''}} value=' '> TEHSIL</option>
                                  <option {{(isset($om_election->location_type) && ' ' == $om_election->location_type) ? 'selected' : ''}} value=' '> BLOCK</option>
                                  <option {{(isset($om_election->location_type) && ' ' == $om_election->location_type) ? 'selected' : ''}} value=' '> VILLAGE</option>
                                  <option {{(isset($om_election->location_type) && ' ' == $om_election->location_type) ? 'selected' : ''}} value=' '> WARD</option>
                                  <option {{(isset($om_election->location_type) && ' ' == $om_election->location_type) ? 'selected' : ''}} value=' '>  COLLEGE</option>
                                  <option {{(isset($om_election->location_type) && 'S' == $om_election->location_type) ? 'selected' : ''}} value='S'>SOCIETY</option>
                                  </select>
                            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Type</label>
                              <select class="form-control" name="type" aria-label="Default select example">
                                  <option selected>Select Type</option><option {{(isset($om_election->type) && 'A' == $om_election->type) ? 'selected' : ''}} value='A'>A</option>
                                  <option {{(isset($om_election->type) && 'V' == $om_election->type) ? 'selected' : ''}} value='V'>V</option>
                                  <option {{(isset($om_election->type) && ' ' == $om_election->type) ? 'selected' : ''}} value=' '> B</option>
                                  <option {{(isset($om_election->type) && ' ' == $om_election->type) ? 'selected' : ''}} value=' '> N</option>
                                  </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Req point</label>
              <input type="text" name="req_point" value="{{isset($om_election->req_point) ? $om_election->req_point : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_election->status) ? $om_election->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_election->ent_on) ? $om_election->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_election->ent_by) ? $om_election->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_election->auth_on) ? $om_election->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_election->auth_by) ? $om_election->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Ward id,</td>
                <td>Ward name</td>
<td>Village id</td>
<td>Status</td>
<td>Ent on</td>
<td>Ent by</td>
<td>Auth on</td>
<td>Auth by</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_ward as $value)
                <tr>
                <td>{{$value->ward_id,}}</td>
                <td>{{$value->ward_name}}</td>
<?php 
                             $temp = Illuminate\Support\Facades\DB::table('om_village')->where('village_id', $value->village_id)->first();


                        ?>
                        <td>{{isset($temp->village_name) ? $temp->village_name : $value->village_id}}</td>
<td>{{$value->status}}</td>
<td>{{$value->ent_on}}</td>
<td>{{$value->ent_by}}</td>
<td>{{$value->auth_on}}</td>
<td>{{$value->auth_by}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_ward/edit/{{$value->ward_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_ward->links() }}
        </div>
        @endsection
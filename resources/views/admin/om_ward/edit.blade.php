@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_ward/{{isset($om_ward) ? "edit/".$om_ward->ward_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Ward name</label>
              <input type="text" name="ward_name" value="{{isset($om_ward->ward_name) ? $om_ward->ward_name : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
                              <label for="userNameInput" class="form-label">Village id</label>
                              <select class="form-control" name="village_id" aria-label="Default select example">
                                  <option selected>Select Village id</option>
                                  @foreach ($om_village as $desc)
                                  <option {{(isset($om_ward->village_id) && $desc->village_id == $om_ward->village_id) ? "selected" : ""}} value="{{$desc->village_id}}">{{$desc->village_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_ward->status) ? $om_ward->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent on</label>
              <input type="text" name="ent_on" value="{{isset($om_ward->ent_on) ? $om_ward->ent_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Ent by</label>
              <input type="text" name="ent_by" value="{{isset($om_ward->ent_by) ? $om_ward->ent_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth on</label>
              <input type="text" name="auth_on" value="{{isset($om_ward->auth_on) ? $om_ward->auth_on : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Auth by</label>
              <input type="text" name="auth_by" value="{{isset($om_ward->auth_by) ? $om_ward->auth_by : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


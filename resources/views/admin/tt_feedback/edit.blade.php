@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/tt_feedback/{{isset($tt_feedback) ? "edit/".$tt_feedback->id : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
                              <label for="userNameInput" class="form-label">User id</label>
                              <select class="form-control" name="user_id" aria-label="Default select example">
                                  <option selected>Select User id</option>
                                  @foreach ($om_user as $desc)
                                  <option {{(isset($tt_feedback->user_id) && $desc->user_id == $tt_feedback->user_id) ? "selected" : ""}} value="{{$desc->user_id}}">{{$desc->user_name}}</option>
                                  @endforeach
                                  
                                </select>
                            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Feedback desc</label>
              <input type="text" name="feedback_desc" value="{{isset($tt_feedback->feedback_desc) ? $tt_feedback->feedback_desc : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($tt_feedback->status) ? $tt_feedback->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


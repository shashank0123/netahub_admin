@extends("admin.layouts.admin")

@section("mainarea")

<form method="POST" action="/admin/om_faq/{{isset($om_faq) ? "edit/".$om_faq->faq_id, : "add"}}" enctype="multipart/form-data">
  @csrf

  <div class="mb-3">
              <label for="userNameInput" class="form-label">Question</label>
              <input type="text" name="question" value="{{isset($om_faq->question) ? $om_faq->question : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Answer</label>
              <input type="text" name="answer" value="{{isset($om_faq->answer) ? $om_faq->answer : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Priority</label>
              <input type="text" name="priority" value="{{isset($om_faq->priority) ? $om_faq->priority : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div><div class="mb-3">
              <label for="userNameInput" class="form-label">Status</label>
              <input type="text" name="status" value="{{isset($om_faq->status) ? $om_faq->status : ""}}" class="form-control" id="userNameInput" aria-describedby="emailHelp">
            </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection


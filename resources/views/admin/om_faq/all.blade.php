@extends('admin.layouts.admin')

        @section('mainarea')
        <div class="container">
            <table class="table bordered">
                <tr>
                <td>Faq id,</td>
                <td>Question</td>
<td>Answer</td>
<td>Priority</td>
<td>Status</td>

                <td>Actions</td>
                </tr>
                @foreach ($om_faq as $value)
                <tr>
                <td>{{$value->faq_id,}}</td>
                <td>{{$value->question}}</td>
<td>{{$value->answer}}</td>
<td>{{$value->priority}}</td>
<td>{{$value->status}}</td>
<td>
                                    <a class='btn btn-primary' href ='/admin/om_faq/edit/{{$value->faq_id,}}'>
                                        <i class='fa fa-pencil'></i>
                                    </a>
                                    <a class='btn btn-danger' href =''>
                                        <i class='fa fa-trash'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </th>
            </table>
            {{ $om_faq->links() }}
        </div>
        @endsection
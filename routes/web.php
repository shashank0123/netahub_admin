<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\UserActionController;
use App\Http\Controllers\WebsiteSettingController;
use App\Http\Controllers\admin\OmBlockController;
            use App\Http\Controllers\admin\OmCollegeController;
            use App\Http\Controllers\admin\OmConstituencyController;
            use App\Http\Controllers\admin\OmCountryController;
            use App\Http\Controllers\admin\OmDistrictController;
            use App\Http\Controllers\admin\OmElectionController;
            use App\Http\Controllers\admin\OmElectionParamController;
            use App\Http\Controllers\admin\OmFaqController;
            use App\Http\Controllers\admin\OmNomineeController;
            use App\Http\Controllers\admin\OmNomineeSurveyController;
            use App\Http\Controllers\admin\OmPartyController;
            use App\Http\Controllers\admin\OmSocietyController;
            use App\Http\Controllers\admin\OmStateController;
            use App\Http\Controllers\admin\OmSurveyController;
            use App\Http\Controllers\admin\OmSurveyConsMapController;
            use App\Http\Controllers\admin\OmTehsilController;
            use App\Http\Controllers\admin\OmUserController;
            use App\Http\Controllers\admin\OmUserConsMapController;
            use App\Http\Controllers\admin\OmUserDeviceController;
            use App\Http\Controllers\admin\OmUserinformationController;
            use App\Http\Controllers\admin\OmVillageController;
            use App\Http\Controllers\admin\OmWardController;
            use App\Http\Controllers\admin\TtFeedbackController;
            use App\Http\Controllers\admin\TtNotificationController;
            use App\Http\Controllers\admin\TtUserPointDetController;
            use App\Http\Controllers\admin\ttUserPointHdrController;
            use App\Http\Controllers\admin\OmParamContentsController;
            // add routes here
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=> 'admin', 'middleware'=> ['admin:admin']], function(){
	Route::get('/login', [AdminController::class, 'getLogin']);
	Route::post('/login', [AdminController::class, 'store'])->name('admin.login');
});

Route::middleware(['auth:sanctum,admin', 'verified'])->get('/admin/dashboard', function () {
    return view('admin.dashboard');
})->name('admin.dashboard');



Route::middleware(['auth:sanctum','web', 'verified'])->get('/dashboard', function () {
    return view('dashboard.dashboard');
})->name('dashboard');


Route::group(['prefix'=>'admin', 'middleware' => ['auth:sanctum,admin', 'verified', ]], function(){
	Route::get('/dashboard', [AdminDashboardController::class, 'index']);
    Route::get('/settings', [WebsiteSettingController::class, 'getSettingPage']);
    Route::get('/om_block', [OmBlockController::class, 'index']);
    Route::get('/om_block/add', [OmBlockController::class, 'create']);
    Route::post('/om_block/add', [OmBlockController::class, 'store']);
    Route::get('/om_block/edit/{id}', [OmBlockController::class, 'edit']);
    Route::post('/om_block/edit/{id}', [OmBlockController::class, 'update']);
    Route::get('/om_college', [OmCollegeController::class, 'index']);
    Route::get('/om_college/add', [OmCollegeController::class, 'create']);
    Route::post('/om_college/add', [OmCollegeController::class, 'store']);
    Route::get('/om_college/edit/{id}', [OmCollegeController::class, 'edit']);
    Route::post('/om_college/edit/{id}', [OmCollegeController::class, 'update']);
    Route::get('/om_constituency', [OmConstituencyController::class, 'index']);
    Route::get('/om_constituency/add', [OmConstituencyController::class, 'create']);
    Route::post('/om_constituency/add', [OmConstituencyController::class, 'store']);
    Route::get('/om_constituency/edit/{id}', [OmConstituencyController::class, 'edit']);
    Route::post('/om_constituency/edit/{id}', [OmConstituencyController::class, 'update']);
    Route::get('/om_country', [OmCountryController::class, 'index']);
    Route::get('/om_country/add', [OmCountryController::class, 'create']);
    Route::post('/om_country/add', [OmCountryController::class, 'store']);
    Route::get('/om_country/edit/{id}', [OmCountryController::class, 'edit']);
    Route::post('/om_country/edit/{id}', [OmCountryController::class, 'update']);
    Route::get('/om_district', [OmDistrictController::class, 'index']);
    Route::get('/om_district/add', [OmDistrictController::class, 'create']);
    Route::post('/om_district/add', [OmDistrictController::class, 'store']);
    Route::get('/om_district/edit/{id}', [OmDistrictController::class, 'edit']);
    Route::post('/om_district/edit/{id}', [OmDistrictController::class, 'update']);
    Route::get('/om_election', [OmElectionController::class, 'index']);
    Route::get('/om_election/add', [OmElectionController::class, 'create']);
    Route::post('/om_election/add', [OmElectionController::class, 'store']);
    Route::get('/om_election/edit/{id}', [OmElectionController::class, 'edit']);
    Route::post('/om_election/edit/{id}', [OmElectionController::class, 'update']);
    Route::get('/om_election_param', [OmElectionParamController::class, 'index']);
    Route::get('/om_election_param/add', [OmElectionParamController::class, 'create']);
    Route::post('/om_election_param/add', [OmElectionParamController::class, 'store']);
    Route::get('/om_election_param/edit/{id}', [OmElectionParamController::class, 'edit']);
    Route::post('/om_election_param/edit/{id}', [OmElectionParamController::class, 'update']);
    Route::get('/om_faq', [OmFaqController::class, 'index']);
    Route::get('/om_faq/add', [OmFaqController::class, 'create']);
    Route::post('/om_faq/add', [OmFaqController::class, 'store']);
    Route::get('/om_faq/edit/{id}', [OmFaqController::class, 'edit']);
    Route::post('/om_faq/edit/{id}', [OmFaqController::class, 'update']);
    Route::get('/om_nominee', [OmNomineeController::class, 'index']);
    Route::get('/om_nominee/add', [OmNomineeController::class, 'create']);
    Route::post('/om_nominee/add', [OmNomineeController::class, 'store']);
    Route::get('/om_nominee/edit/{id}', [OmNomineeController::class, 'edit']);
    Route::post('/om_nominee/edit/{id}', [OmNomineeController::class, 'update']);
    Route::get('/om_nominee_survey', [OmNomineeSurveyController::class, 'index']);
    Route::get('/om_nominee_survey/add', [OmNomineeSurveyController::class, 'create']);
    Route::post('/om_nominee_survey/add', [OmNomineeSurveyController::class, 'store']);
    Route::get('/om_nominee_survey/edit/{id}', [OmNomineeSurveyController::class, 'edit']);
    Route::post('/om_nominee_survey/edit/{id}', [OmNomineeSurveyController::class, 'update']);
    Route::get('/om_party', [OmPartyController::class, 'index']);
    Route::get('/om_party/add', [OmPartyController::class, 'create']);
    Route::post('/om_party/add', [OmPartyController::class, 'store']);
    Route::get('/om_party/edit/{id}', [OmPartyController::class, 'edit']);
    Route::post('/om_party/edit/{id}', [OmPartyController::class, 'update']);
    Route::get('/om_society', [OmSocietyController::class, 'index']);
    Route::get('/om_society/add', [OmSocietyController::class, 'create']);
    Route::post('/om_society/add', [OmSocietyController::class, 'store']);
    Route::get('/om_society/edit/{id}', [OmSocietyController::class, 'edit']);
    Route::post('/om_society/edit/{id}', [OmSocietyController::class, 'update']);
    Route::get('/om_state', [OmStateController::class, 'index']);
    Route::get('/om_state/add', [OmStateController::class, 'create']);
    Route::post('/om_state/add', [OmStateController::class, 'store']);
    Route::get('/om_state/edit/{id}', [OmStateController::class, 'edit']);
    Route::post('/om_state/edit/{id}', [OmStateController::class, 'update']);
    Route::get('/om_survey', [OmSurveyController::class, 'index']);
    Route::get('/om_survey/add', [OmSurveyController::class, 'create']);
    Route::post('/om_survey/add', [OmSurveyController::class, 'store']);
    Route::get('/om_survey/edit/{id}', [OmSurveyController::class, 'edit']);
    Route::post('/om_survey/edit/{id}', [OmSurveyController::class, 'update']);
    Route::get('/om_survey_cons_map', [OmSurveyConsMapController::class, 'index']);
    Route::get('/om_survey_cons_map/add', [OmSurveyConsMapController::class, 'create']);
    Route::post('/om_survey_cons_map/add', [OmSurveyConsMapController::class, 'store']);
    Route::get('/om_survey_cons_map/edit/{id}', [OmSurveyConsMapController::class, 'edit']);
    Route::post('/om_survey_cons_map/edit/{id}', [OmSurveyConsMapController::class, 'update']);
    Route::get('/om_tehsil', [OmTehsilController::class, 'index']);
    Route::get('/om_tehsil/add', [OmTehsilController::class, 'create']);
    Route::post('/om_tehsil/add', [OmTehsilController::class, 'store']);
    Route::get('/om_tehsil/edit/{id}', [OmTehsilController::class, 'edit']);
    Route::post('/om_tehsil/edit/{id}', [OmTehsilController::class, 'update']);
    Route::get('/om_user', [OmUserController::class, 'index']);
    Route::get('/om_user/add', [OmUserController::class, 'create']);
    Route::post('/om_user/add', [OmUserController::class, 'store']);
    Route::get('/om_user/edit/{id}', [OmUserController::class, 'edit']);
    Route::post('/om_user/edit/{id}', [OmUserController::class, 'update']);
    Route::get('/om_user_cons_map', [OmUserConsMapController::class, 'index']);
    Route::get('/om_user_cons_map/add', [OmUserConsMapController::class, 'create']);
    Route::post('/om_user_cons_map/add', [OmUserConsMapController::class, 'store']);
    Route::get('/om_user_cons_map/edit/{id}', [OmUserConsMapController::class, 'edit']);
    Route::post('/om_user_cons_map/edit/{id}', [OmUserConsMapController::class, 'update']);
    Route::get('/om_user_device', [OmUserDeviceController::class, 'index']);
    Route::get('/om_user_device/add', [OmUserDeviceController::class, 'create']);
    Route::post('/om_user_device/add', [OmUserDeviceController::class, 'store']);
    Route::get('/om_user_device/edit/{id}', [OmUserDeviceController::class, 'edit']);
    Route::post('/om_user_device/edit/{id}', [OmUserDeviceController::class, 'update']);
    Route::get('/om_user_information', [OmUserinformationController::class, 'index']);
    Route::get('/om_user_information/add', [OmUserinformationController::class, 'create']);
    Route::post('/om_user_information/add', [OmUserinformationController::class, 'store']);
    Route::get('/om_user_information/edit/{id}', [OmUserinformationController::class, 'edit']);
    Route::post('/om_user_information/edit/{id}', [OmUserinformationController::class, 'update']);
    Route::get('/om_village', [OmVillageController::class, 'index']);
    Route::get('/om_village/add', [OmVillageController::class, 'create']);
    Route::post('/om_village/add', [OmVillageController::class, 'store']);
    Route::get('/om_village/edit/{id}', [OmVillageController::class, 'edit']);
    Route::post('/om_village/edit/{id}', [OmVillageController::class, 'update']);
    Route::get('/om_ward', [OmWardController::class, 'index']);
    Route::get('/om_ward/add', [OmWardController::class, 'create']);
    Route::post('/om_ward/add', [OmWardController::class, 'store']);
    Route::get('/om_ward/edit/{id}', [OmWardController::class, 'edit']);
    Route::post('/om_ward/edit/{id}', [OmWardController::class, 'update']);
    Route::get('/tt_feedback', [TtFeedbackController::class, 'index']);
    Route::get('/tt_feedback/add', [TtFeedbackController::class, 'create']);
    Route::post('/tt_feedback/add', [TtFeedbackController::class, 'store']);
    Route::get('/tt_feedback/edit/{id}', [TtFeedbackController::class, 'edit']);
    Route::post('/tt_feedback/edit/{id}', [TtFeedbackController::class, 'update']);
    Route::get('/tt_notification', [TtNotificationController::class, 'index']);
    Route::get('/tt_notification/add', [TtNotificationController::class, 'create']);
    Route::post('/tt_notification/add', [TtNotificationController::class, 'store']);
    Route::get('/tt_notification/edit/{id}', [TtNotificationController::class, 'edit']);
    Route::post('/tt_notification/edit/{id}', [TtNotificationController::class, 'update']);
    Route::get('/tt_user_point_det', [TtUserPointDetController::class, 'index']);
    Route::get('/tt_user_point_det/add', [TtUserPointDetController::class, 'create']);
    Route::post('/tt_user_point_det/add', [TtUserPointDetController::class, 'store']);
    Route::get('/tt_user_point_det/edit/{id}', [TtUserPointDetController::class, 'edit']);
    Route::post('/tt_user_point_det/edit/{id}', [TtUserPointDetController::class, 'update']);
    Route::get('/tt_user_point_hdr', [ttUserPointHdrController::class, 'index']);
    Route::get('/tt_user_point_hdr/add', [ttUserPointHdrController::class, 'create']);
    Route::post('/tt_user_point_hdr/add', [ttUserPointHdrController::class, 'store']);
    Route::get('/tt_user_point_hdr/edit/{id}', [ttUserPointHdrController::class, 'edit']);
    Route::post('/tt_user_point_hdr/edit/{id}', [ttUserPointHdrController::class, 'update']);
    Route::get('/om_param_contents', [OmParamContentsController::class, 'index']);
    Route::get('/om_param_contents/add', [OmParamContentsController::class, 'create']);
    Route::post('/om_param_contents/add', [OmParamContentsController::class, 'store']);
    Route::get('/om_param_contents/edit/{id}', [OmParamContentsController::class, 'edit']);
    Route::post('/om_param_contents/edit/{id}', [OmParamContentsController::class, 'update']);
    //Admin routes to be added here
});

Route::group(['prefix'=>'members', 'middleware' => ['auth:sanctum', 'verified', ]], function(){
	// Route::get('/dashboard', [AdminDashboardController::class, 'index']);
	Route::get('/profile', [UserActionController::class, 'getProfile'])->name('user.profile');
	Route::get('/profile/edit', [UserActionController::class, 'editProfileForm'])->name('user.profile.edit');
	Route::post('/profile/edit', [UserActionController::class, 'updateProfile'])->name('user.profile.update');
	Route::get('/password', [UserActionController::class, 'editPasswordForm'])->name('user.password.view');
	Route::post('/password', [UserActionController::class, 'updatePassword'])->name('user.password.update');
});
Route::get('/user/logout', [UserActionController::class, 'logout'])->name('user.logout');

Route::get('/admin/logout', [AdminController::class, 'destroy'])->name('admin.logout');
